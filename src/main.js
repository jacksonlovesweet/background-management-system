import Vue from 'vue'
import App from './App.vue'
import './assets/css/global.css'
//禁用按钮弹出框样式修改
import "./assets/css/popover.css"
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import router from './router'
//vuex
import store from '@/store'
//富文本
import VueQuillEditor from 'vue-quill-editor';
// 引入全局样式
import '@/style/common.css'
//  require styles 引入样式
import 'quill/dist/quill.core.css';
import 'quill/dist/quill.snow.css';
import 'quill/dist/quill.bubble.css';
// ant
// import Antd from 'ant-design-vue';
// import 'ant-design-vue/dist/antd.css';
// import {
//   Button
// } from 'ant-design-vue';
// api
import Api from '@/api'

// 判断按钮权限3.0
import { showBtn } from "@/utils/tools.js"
Vue.prototype._SHOWBTN = showBtn
    // import './components/editor/editor-icon.css'
    //MD5加密
import md5 from 'js-md5' //引入
Vue.prototype.$md5 = md5; //定义全局变量
// 百度地图
import BaiduMap from 'vue-baidu-map'

// echarts
import echarts from 'echarts'
Vue.prototype.$echarts = echarts
import './components/editor/editor-icon.css'
Vue.config.productionTip = false
    // Vue.use(Button);
Vue.prototype.Api = Api
Vue.use(ElementUI);
// Vue.use(Antd);
//富文本编辑器
Vue.use(VueQuillEditor)
Vue.use(BaiduMap, {
    // ak 是在百度地图开发者平台申请的密钥 详见 http://lbsyun.baidu.com/apiconsole/key */
    ak: 'dlK3jfHX0N3uQSoneg950bkA4nIKnX7Z'
})
new Vue({
    router,
    store,
    render: h => h(App),
}).$mount('#app')