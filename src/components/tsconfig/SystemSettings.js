// 系统设置
export default [
  {
    route: "/index/SystemSettings/Organization",
    title: '组织机构',
    code:"xtsz-zzjg"
  },
  {
    route: "/index/SystemSettings/Role",
    title: '角色管理',
    code:"xtsz-jsgl"

  },
  {
    route: "/index/SystemSettings/User",
    title: '用户管理',
    code:"xtsz-yhgl"

  },
  {
    route: "/index/SystemSettings/Personage",
    title: '个人中心',
    code:"important_grzx"
  },
  {
    route: "/index/SystemSettings/Dictionaries",
    title: '字典管理',
    code:"xtsz-zdgl"

  },
  {
    route: "/index/SystemSettings/Sun",
    title: '阳光公正',
    code:"",

  },
]
