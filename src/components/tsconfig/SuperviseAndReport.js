// 监督举报
export default [{
        route: "/DynamicMonitoring",
        title: '动态监督',
        code: "jdjb-dtjd",
        children: [{
                route: "/index/SuperviseAndReport/Expenditure",
                title: '三公经费',
                code: "jdjb-dtjd-sgxf",
            },
            {
                route: "/index/SuperviseAndReport/Conference",
                title: '三重一大',
                code: "jdjb-dtjd-szyd_manage",
            },
            {
                route: "/index/SuperviseAndReport/Conference",
                title: '三重一大',
                code: "jdjb-dtjd-szyd_upload",
            },
            {
                route: "/index/SuperviseAndReport/Finance",
                title: '财务监督',
                code: "jdjb-dtjd-cwjd",
            },
        ]
    },
    {
        route: "/SpecialSupervision",
        title: '专项监督',
        code: "important_grzx",
        children: [{
                route: "/index/SuperviseAndReport/Task",
                title: '任务管理',
                code: "important_grzx",
            },
            {
                route: "/index/SuperviseAndReport/Report",
                title: '情况报告',
                code: "important_grzx",
            },
        ]
    },
    {
        route: "/Daily",
        title: '日常监督',
        code: "jdjb-rcjd",
        children: [{
                route: "/index/SuperviseAndReport/Notification",
                title: '情况通报',
                code: "jdjb-rcjd-qktb_manage",
            },
            {
                route: "/index/SuperviseAndReport/Notification",
                title: '情况通报',
                code: "jdjb-rcjd-qktb_upload",
            },
            {
                route: "/index/SuperviseAndReport/Neaten",
                title: '整改汇报',
                code: "jdjb-rcjd-zghb",
            },
        ]
    },
    {
        route: "/index/SuperviseAndReport/PetitionReport",
        title: '信访举报',
        code: "jdjb-xfjb",
    },
    {
        route: "/index/SuperviseAndReport/Instructions",
        title: '请示报告',
        code: "important_grzx",
    },

]