// 门户管理
export default [
  {
    route: "/index/PortalManagement/Home",
    title: '首页',
    code: "wzgl-sy"
  },
  {
    route: "/index/PortalManagement/Introduction",
    title: '系统简介',
    code: "wzgl-xtjj"
  },
  {
    route: "/index/PortalManagement/Archives",
    title: ' 组织机构',
    code: "wzgl-lzda"
  },
  {
    route: "4",
    title: '监督举报',
    code: "wzgl-jdjb",
    children: [
      {
        route: "/index/PortalManagement/ReportOnInternet",
        title: '网上举报',
        code: "wzgl-jdjb-wsjb"
      },
      {
        route: "/index/PortalManagement/QRReport",
        title: '二维码举报',
        code: "wzgl-jdjb-ewmjb"

      },
      {
        route: "/index/PortalManagement/InvestigationAndEvaluation",
        title: '调查评估',
        code: "wzgl-jdjb-dcpg"
      },
    ]
  },
  {
    route: "5",
    title: '清风论坛',
    code: "wzgl-cflt",
    children: [
      {
        route: "/index/PortalManagement/Classroom",
        title: '亦廉课堂',
        code: "wzgl-cflt-ylkt",

      },
      {
        route: "/index/PortalManagement/OutsideBox",
        code: "wzgl-cflt-tszs",
        title: '他山之石',
      },
      {
        route: "/index/PortalManagement/Warning",
        code: "wzgl-cflt-jsjy",
        title: '警示教育',
      },
    ]
  },
  {
    route: "6",
    code: "wzgl-fxfk",
    title: '风险防控',
    children: [
      {
        route: "/index/PortalManagement/Risk",
        code: "wzgl-fxfk-sxddfx",
        title: '思想道德风险',
      },
      // {
      //   route: "/index/PortalManagement/Home",
      //   title: '制度机制风险',
      // },
      // {
      //   route: "/index/PortalManagement/Home",
      //   title: '岗位责任风险',
      // },
    ]
  },
  {
    route: "7",
    code: "wzgl-djfg",
    title: '党纪法规',
    children: [
      {
        route: "/index/PortalManagement/Statute",
        code: "wzgl-djfg-wsjb",
        title: '分类系统',
      },
      {
        route: "/index/PortalManagement/Tips",
        code: "wzgl-djfg-xts",
        title: '小贴士',
      },
    ]
  },
  {
    route: "8",
    code: "wzgl-xsxc",
    title: '巡视巡察',
    children: [
      {
        route: "/index/PortalManagement/Inspection",
        code: "wzgl-xsxc-zydt",
        title: '中央动态',
      },
      {
        route: "/index/PortalManagement/BeiJing",
        code: "wzgl-xsxc-bjdt",
        title: '北京动态',
      },
      {
        route: "/index/PortalManagement/Group",
        code: "wzgl-xsxc-jtdt",
        title: '集团动态',
      },
      {
        route: "/index/PortalManagement/Special",
        code: "wzgl-xsxc-zt",
        title: '专题维护',
      },
    ]
  },
  {
    route: "9",
    code: "wzgl-scdc",
    title: '审查调查',
    children: [
      // {
      //   route: "/index/PortalManagement/Investigation",
      //   title: '执纪审查',
      // },
      // {
      //   route: "/index/PortalManagement/Home",
      //   title: '执法审查',
      // },
      {
        route: "/index/PortalManagement/PD",
        code: "wzgl-scdc-djzwcf",
        title: '党纪政务处分',
      },
    ]
  },




]
