//  廉政档案
export default [
  {
    route: "/index/IncorruptibleArchives/supervise",
    title: '监督对象',
    code:"lzda-jddx"
  },
  {
    route: "/index/IncorruptibleArchives/supervision", title: '监察对象',
    code:"lzda-jcdx"

  },
  {
    route: "/index/IncorruptibleArchives/archives", title: '公司档案',
    code:"lzda-gsda"

  },

]
