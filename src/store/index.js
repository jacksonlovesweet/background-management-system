import Vue from 'vue'
import Vuex from 'vuex'
//廉政档案
import IncorruptibleArchives from './modules/IncorruptibleArchives/index.js';
//巡视巡察
import Patrol from './modules/Patrol/index.js';
//门户管理
import PortalManagement from './modules/PortalManagement/index.js';
//监督举报
import SuperviseAndReport from './modules/SuperviseAndReport/index.js';
//系统设置
import SystemSettings from './modules/SystemSettings/index.js';
import PublicData from './modules/PublicData/index.js';
Vue.use(Vuex);
export default new Vuex.Store({
    modules: {
        IncorruptibleArchives: {...IncorruptibleArchives, namespaced: true },
        Patrol,
        PortalManagement,
        SuperviseAndReport,
        SystemSettings,
        PublicData
    },
})