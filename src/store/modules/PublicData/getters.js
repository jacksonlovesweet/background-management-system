export default {
    // 获取配置路由 keep-alive 的值
    GetComponentAlive: (state, getters) => (componentName) => {
        return state.aliveComponents[componentName] || false
    }
}