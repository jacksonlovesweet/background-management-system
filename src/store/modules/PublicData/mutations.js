export default {
    // 配置 keep - alive 的路由
    SetComponentAlive(state, componentName, isAlive) {
        state.aliveComponents[componentName] = isAlive
    }
}