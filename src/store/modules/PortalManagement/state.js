export default {
    //图片根路径
    fileIp: "",
    // 控制网上举报，页面跳转返回时的tab选中问题
    reportIsActive:"first",
    // 控制武装自己，页面跳转返回时的tab选中问题
    outsideBoxActive:"first",
    // 控制首页，页面跳转返回时的tab选中问题
    HomeActive:"first",
    //权限的所有code码集合
    PermissionCode:[],
    //控制分液器的页数和条数
    pageNum:1,
    pageSize:10,

}