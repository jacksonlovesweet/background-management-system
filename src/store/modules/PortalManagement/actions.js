import api from "@/api"

export default {
    SET_FILEIP({ commit }) {
        return new Promise((resolve) => {
            api.getIp().then(res => {
                commit('SET_FILEIP', res.data)
                resolve();
            });
        })
    },
    SET_PERMISSION_CODE({ commit }) {
        return new Promise((resolve) => {
            api.PermissionCode().then(res => {
                commit('SET_PERMISSION_CODE', res.data.resList)
                resolve();
            });
        })
    },
}