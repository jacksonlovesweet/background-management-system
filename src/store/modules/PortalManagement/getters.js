export default {
    getPermissionBycode: (state, getters) => (code) => {
        
        let allCode = state.PermissionCode.filter(item => item.indexOf(code) !== -1)

        if (allCode.includes(code + '_manage')) {
            return allCode.filter(item => item.indexOf(code + '_manage') !== -1)
        } else if (allCode.includes(code + '_upload')) {
            return allCode.filter(item => item.indexOf(code + '_upload') !== -1)
        } else {
            return allCode
        }
    }
}