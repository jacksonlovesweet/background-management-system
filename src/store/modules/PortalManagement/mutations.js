export default {
    // 图片根路径
    SET_FILEIP(state, fileIp) {
        state.fileIp = fileIp
        // console.log(state.fileIp,222)
    },
    set_reportIsActive(state, reportIsActive){
        state.reportIsActive = reportIsActive
        // console.log(state.reportIsActive,"reportIsActive")
    },
    SET_OUTSIDE_BOX_ACTIVE(state,outsideBoxActive){
        state.outsideBoxActive = outsideBoxActive
    },
    HOME_ACTIVE(state,HomeActive){
        state.HomeActive = HomeActive
    },
    //权限的所有code码集合
    SET_PERMISSION_CODE(state,Code){
        state.PermissionCode = Code
        // console.log(state.PermissionCode,"权限的所有code码集合")
    },
    //页数
    SET_PAGE_NUM(state,pageNum){
        state.pageNum = pageNum
    },
    //条数
    SET_PAGE_SIZE(state,pageSize){
        state.pageSize = pageSize
    },
}