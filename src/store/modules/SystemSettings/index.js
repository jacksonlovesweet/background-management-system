import state from './state.js';
import mutations from './mutations.js';
// import getters from './getters.js';


export default{
    namespaced: true,
//  将导入的文件暴露出去
	state,
    mutations,
}


