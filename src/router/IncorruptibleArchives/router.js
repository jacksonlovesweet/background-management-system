import IncorruptibleArchives from "@/views/IncorruptibleArchives/index.vue"
//监督对象
import supervise from "@/views/IncorruptibleArchives/supervise/index.vue"
//监督对象-主体
import supervisemain from "@/views/IncorruptibleArchives/supervise/supervisemain/index.vue"
//监督对象-详情
import superviseDetail from "@/views/IncorruptibleArchives/supervise/superviseDetail.vue"
//监督对象新建页面
import SuperviseNewbuild from "@/views/IncorruptibleArchives/supervise/SuperviseNewbuild.vue"
//监督对象详情页档案列表页
import ArchivesList from "@/views/IncorruptibleArchives/supervise/ArchivesList.vue"
//监督对象详情页档案列表编辑页
import ArchivesEdit from "@/views/IncorruptibleArchives/supervise/ArchivesEdit.vue"
//七种新建编辑查看，述责述廉报告
import ResponsibilityAndIntegrity from "@/views/IncorruptibleArchives/SevenTables/ResponsibilityAndIntegrity.vue"
//七种新建编辑查看，操办婚丧喜庆事宜报告表
import OperationalMmatters from "@/views/IncorruptibleArchives/SevenTables/OperationalMmatters.vue"
//七种新建编辑查看，领导干部受组织处理和党政纪处分情况登记表
import PunishmentList from "@/views/IncorruptibleArchives/SevenTables/PunishmentList.vue"
//七种新建编辑查看，礼品情况登记表
import GiftRegistration from "@/views/IncorruptibleArchives/SevenTables/GiftRegistration.vue"
//七种新建编辑查看，礼品情况登记表
import TurnoverForm from "@/views/IncorruptibleArchives/SevenTables/TurnoverForm.vue"
//七种新建编辑查看，民主生活会对照检查材料
import DemocraticLife from "@/views/IncorruptibleArchives/SevenTables/DemocraticLife.vue"
//监督对象的个人信息详情查看
import detailChange from "@/views/IncorruptibleArchives/SevenTables/detailChange.vue"
//监督对象的个人信息详情--详细信息
import detailPersonal from "@/views/IncorruptibleArchives/SevenTables/detailPersonal.vue"
// 信访更多
import AddLetter from "@/views/IncorruptibleArchives/supervise/supervisemain/AddLetter.vue"
// 信访查看
import LetterLookO from "@/views/IncorruptibleArchives/supervise/supervisemain/LetterLookO.vue"
//-------------------------
//监察对象
import supervision from "@/views/IncorruptibleArchives/supervision/index.vue"
//监察对象-主体
import supervisionmain from "@/views/IncorruptibleArchives/supervision/supervisionmain/index.vue"
//监察对象-详情
import supervisionDetail from "@/views/IncorruptibleArchives/supervision/supervisionDetail.vue"
//公司档案
import archives from "@/views/IncorruptibleArchives/archives/index.vue"
//公司档案-主体
import archivesmain from "@/views/IncorruptibleArchives/archives/archivesmain/index.vue"
// 公司档案详情
import Detail from "@/views/IncorruptibleArchives/archives/archivesmain/Detail.vue"
// 公司档案详情/详情
import InformationDetails from "@/views/IncorruptibleArchives/archives/archivesmain/InformationDetails.vue"
// 公司档案修改
import EditDetail from "@/views/IncorruptibleArchives/archives/archivesmain/EditDetail.vue"
// 档案列表新建
import AddArchives from "@/views/IncorruptibleArchives/archives/archivesmain/AddArchives.vue"
// 档案列表更多
import ArchivesMore from "@/views/IncorruptibleArchives/archives/archivesmain/ArchivesMore.vue"
// 档案列表更多
import Look from "@/views/IncorruptibleArchives/archives/archivesmain/Look.vue"
// 专项监督更多
import LetterMore from "@/views/IncorruptibleArchives/archives/archivesmain/LetterMore.vue"
// 信访更多
import Letter from "@/views/IncorruptibleArchives/archives/archivesmain/Letter.vue"
// 日常更多
import Daily from "@/views/IncorruptibleArchives/archives/archivesmain/Daily.vue"
// 巡视巡查看
import SuperviseMore from "@/views/IncorruptibleArchives/archives/archivesmain/SuperviseMore.vue"
// 巡视巡查更多
import SurMore from "@/views/IncorruptibleArchives/archives/archivesmain/SurMore.vue"
//信访查看
import LetterLook from "@/views/IncorruptibleArchives/archives/archivesmain/LetterLook.vue"
//专项监督查看
import SpecialLook from "@/views/IncorruptibleArchives/archives/archivesmain/SpecialLook.vue"
// 日常监督查看
import DailyLook from "@/views/IncorruptibleArchives/archives/archivesmain/DailyLook.vue"
export default [
    {
        name: "IncorruptibleArchives",
        path: "IncorruptibleArchives",
        component: IncorruptibleArchives,
        redirect: {name:"supervise"},
        meta: {
            title: '廉政档案',
            auth: true,
            code:"lzda"
        },
        children: [
            {
                path: "supervise",
                name: "supervise",
                meta: {
                    title: '监督对象',
                    auth: true,
                    code:"lzda-jddx"
                },
                component: supervise,
                redirect: {name:"supervisemain"},
                code:"lzda-jddx",
                children: [
                    {
                        path: "supervisemain",
                        name: "supervisemain",
                        meta: {
                            title: '监督对象主体',
                            auth: true,
                            code:"lzda-jddx"
                        },
                        component: supervisemain,
                    },
                    {
                        path: "supervisedetail",
                        name: "supervisedetail",
                        meta: {
                            title: '监督对象详情',
                            auth: true,
                            code:"lzda-jddx"
                        },
                        component: superviseDetail,
                    },
                    {
                        path: "SuperviseNewbuild",
                        name: "SuperviseNewbuild",
                        meta: {
                            title: '监督对象新建',
                            auth: true,
                            code:"lzda-jddx"
                        },
                        component: SuperviseNewbuild,
                    },
                    {
                        path: "ArchivesList",
                        name: "ArchivesList",
                        meta: {
                            title: '监督对象详情档案列表更多',
                            auth: true,
                            code:"lzda-jddx"
                        },
                        component: ArchivesList,
                    },
                    {
                        path: "ArchivesEdit",
                        name: "ArchivesEdit",
                        meta: {
                            title: '监督对象详情档案列表编辑',
                            auth: true,
                            code:"lzda-jddx"
                        },
                        component: ArchivesEdit,
                    },
                    {
                        path: "ResponsibilityAndIntegrity",
                        name: "ResponsibilityAndIntegrity",
                        meta: {
                            title: '监督对象述责述廉报告',
                            auth: true,
                            code:"lzda-jddx"
                        },
                        component: ResponsibilityAndIntegrity,
                    },
                    {
                        path: "OperationalMmatters",
                        name: "OperationalMmatters",
                        meta: {
                            title: '监督对象操办事宜报告表',
                            auth: true,
                            code:"lzda-jddx"
                        },
                        component: OperationalMmatters,
                    },
                    {
                        path: "PunishmentList",
                        name: "PunishmentList",
                        meta: {
                            title: '监督对象处分情况登记表',
                            auth: true,
                            code:"lzda-jddx"
                        },
                        component: PunishmentList,
                    },
                    {
                        path: "GiftRegistration",
                        name: "GiftRegistration",
                        meta: {
                            title: '监督对象礼品情况登记表',
                            auth: true,
                            code:"lzda-jddx"
                        },
                        component: GiftRegistration,
                    },
                    {
                        path: "TurnoverForm",
                        name: "TurnoverForm",
                        meta: {
                            title: '监督对象离职交接表',
                            auth: true,
                            code:"lzda-jddx"
                        },
                        component: TurnoverForm,
                    },
                    {
                        path: "AddLetter",
                        name: "AddLetter",
                        meta: {
                            title: '监督对象礼品情况登记表',
                            auth: true,
                            code:"lzda-jddx"
                        },
                        component: AddLetter,
                    },
                    {
                        path: "LetterLookO",
                        name: "LetterLookO",
                        meta: {
                            title: '信访查看',
                            auth: true,
                            code:"lzda-jddx"
                        },
                        component:LetterLookO,
                    },
                    {
                        path: "DemocraticLife",
                        name: "DemocraticLife",
                        meta: {
                            title: '民主生活会',
                            auth: true,
                            code:"lzda-jddx"
                        },
                        component:DemocraticLife,
                    },
                    {
                        path: "detailChange",
                        name: "detailChange",
                        meta: {
                            title: '监督对象详情-个人信息修改',
                            auth: true,
                            code:"lzda-jddx"
                        },
                        component:detailChange,
                    },
                    {
                        path: "detailPersonal",
                        name: "detailPersonal",
                        meta: {
                            title: '监督对象详情-个人信息详情',
                            auth: true,
                            code:"lzda-jddx"
                        },
                        component:detailPersonal,
                    },
                    

                ]
            },
            {
                path: "supervision",
                name: "supervision",
                meta: {
                    title: '监察对象',
                    auth: true,
                    code:"lzda-jcdx"
                },
                component: supervision,
                redirect: '/index/IncorruptibleArchives/supervision/supervisionmain',
                children: [
                    {
                        path: "supervisionmain",
                        name: "supervisionmain",
                        meta: {
                            title: '监察对象主体',
                            auth: true,
                            code:"lzda-jcdx"
                        },
                        component: supervisionmain,
                    },
                    {
                        path: "supervisiondetail",
                        name: "supervisiondetail",
                        meta: {
                            title: '监察对象详情',
                            auth: true,
                            code:"lzda-jcdx"
                        },
                        component: supervisionDetail,
                    },
                    
                ]
            },
            {
                path: "archives",
                name: "archives",
                meta: {
                    title: '公司档案',
                    auth: true,
                    code:"lzda-gsda"
                },
                component: archives,
                redirect: '/index/IncorruptibleArchives/archives/archivesmain',
                children: [
                    {
                        path: "archivesmain",
                        name: "archivesmain",
                        meta: {
                            title: '公司档案主体',
                            auth: true,
                            code:"lzda-gsda"
                        },
                        component: archivesmain,
                    },
                    {
                        path: "Detail",
                        name: "Detail",
                        meta: {
                            title: '公司档案详情',
                            auth: true,
                            code:"lzda-gsda"
                        },
                        component: Detail,
                    },
                    {
                        path: "InformationDetails",
                        name: "InformationDetails",
                        meta: {
                            title: '公司档案详情/详情',
                            auth: true,
                            code:"lzda-gsda"
                        },
                        component: InformationDetails,
                    },
                    {
                        path: "EditDetail",
                        name: "EditDetail",
                        meta: {
                            title: '公司档案详情编辑',
                            auth: true,
                            code:"lzda-gsda"
                        },
                        component: EditDetail,
                    },
                    {
                        path: "AddArchives",
                        name: "AddArchives",
                        meta: {
                            title: '档案列表新建',
                            auth: true,
                            code:"lzda-gsda"
                        },
                        component: AddArchives,
                    },
                    {
                        path: "ArchivesMore",
                        name: "ArchivesMore",
                        meta: {
                            title: '档案列表更多',
                            auth: true,
                            code:"lzda-gsda"
                        },
                        component: ArchivesMore,
                    },
                    {
                        path: "Look",
                        name: "Look",
                        meta: {
                            title: '档案列表查看',
                            auth: true,
                            code:"lzda-gsda"
                        },
                        component: Look,
                    },
                    {
                        path: "LetterMore",
                        name: "LetterMore",
                        meta: {
                            title: '专项监督更多',
                            auth: true,
                            code:"lzda-gsda"
                        },
                        component:LetterMore,
                    },
                    {
                        path: "Letter",
                        name: "Letter",
                        meta: {
                            title: '信访举报更多',
                            auth: true,
                            code:"lzda-gsda"
                        },
                        component:Letter,
                    },
                    {
                        path: "Daily",
                        name: "Daily",
                        meta: {
                            title: '日常监督更多',
                            auth: true,
                            code:"lzda-gsda"
                        },
                        component:Daily,
                    },
                    {
                        path: "SuperviseMore",
                        name: "SuperviseMore",
                        meta: {
                            title: '日常监督查看',
                            auth: true,
                            code:"lzda-gsda"
                        },
                        component:SuperviseMore,
                    },
                    {
                        path: "SurMore",
                        name: "SurMore",
                        meta: {
                            title: '日常监督更多',
                            auth: true,
                            code:"lzda-gsda"
                        },
                        component:SurMore,
                    },
                    {
                        path: "LetterLook",
                        name: "LetterLook",
                        meta: {
                            title: '日常监督更多',
                            auth: true,
                            code:"lzda-gsda"
                        },
                        component:LetterLook,
                    },
                    {
                        path: "SpecialLook",
                        name: "SpecialLook",
                        meta: {
                            title: '专项监督查看',
                            auth: true,
                            code:"lzda-gsda"
                        },
                        component:SpecialLook,
                    },
                    {
                        path: "DailyLook",
                        name: "DailyLook",
                        meta: {
                            title: '日常监督查看',
                            auth: true,
                            code:"lzda-gsda"
                        },
                        component:DailyLook,
                    },
                ]
            },
        ]
    },
]