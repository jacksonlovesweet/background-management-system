//门户管理
import PortalManagement from "@/views/PortalManagement/index.vue"
//档案
import Archives from "@/views/PortalManagement/Archives/index.vue"
//清风论坛
import Classroom from "@/views/PortalManagement/Forum/Classroom/index.vue"
import OutsideBox from "@/views/PortalManagement/Forum/OutsideBox/index.vue"
import Warning from "@/views/PortalManagement/Forum/Warning/index.vue"
import WarningTextEditor from "@/views/PortalManagement/Forum/Warning/WarningTextEditor.vue"
import ClassTextEditor from "@/views/PortalManagement/Forum/Classroom/TextClass.vue"
import GoBackClass from "@/views/PortalManagement/Forum/Classroom/GobackClass.vue"
import BianJiClass from "@/views/PortalManagement/Forum/Classroom/BianJiClass.vue"
import BianJiForum from "@/views/PortalManagement/Forum/OutsideBox/BianJiForum.vue"
import BuildNewForum from "@/views/PortalManagement/Forum/OutsideBox/BuildNewForum.vue"
import GobackArmed from "@/views/PortalManagement/Forum/OutsideBox/GobackArmed.vue"

//首页
import Home from "@/views/PortalManagement/Home/index.vue"
import TextEditor from "@/views/PortalManagement/Home/BannerConfig/TextEditor.vue"
import GoBack from "@/views/PortalManagement/Home/BannerConfig/GoBack.vue"
import BianJi from "@/views/PortalManagement/Home/BannerConfig/BianJi.vue"

//巡查巡视
import Inspection from "@/views/PortalManagement/Inspection/index.vue"
import BeiJing from "@/views/PortalManagement/Inspection/BeiJing.vue"
import BJXJ from "@/views/PortalManagement/Inspection/BJXJ.vue"
import BJBJ from "@/views/PortalManagement/Inspection/BJBJ.vue"
import Group from "@/views/PortalManagement/Inspection/Group.vue"
import GroupXj from "@/views/PortalManagement/Inspection/GroupXj.vue"
import GroupBj from "@/views/PortalManagement/Inspection/GroupBj.vue"
import Special from "@/views/PortalManagement/Inspection/Special.vue"
import SpecialXj from "@/views/PortalManagement/Inspection/SpecialXj.vue"
import SpecialBj from "@/views/PortalManagement/Inspection/SpecialBj.vue"
import ZH from "@/views/PortalManagement/Inspection/ZH.vue"
import ZHBJ from "@/views/PortalManagement/Inspection/ZHBJ.vue"
//系统简介
import Introduction from "@/views/PortalManagement/Introduction/index.vue"
//审查调查
import Investigation from "@/views/PortalManagement/Investigation/index.vue"
import PD from "@/views/PortalManagement/Investigation/PD.vue"
import PDXJ from "@/views/PortalManagement/Investigation/PDXJ.vue"
import PDBJ from "@/views/PortalManagement/Investigation/PDBJ.vue"
//风险防控
import Risk from "@/views/PortalManagement/Risk/index.vue"
import Sx from "@/views/PortalManagement/Risk/Sx.vue"
import SxXj from "@/views/PortalManagement/Risk/SxXj.vue"
import SxBj from "@/views/PortalManagement/Risk/SxBj.vue"
//党纪法规
import Statute from "@/views/PortalManagement/Statute/index.vue"
import FlXj from "@/views/PortalManagement/Statute/FlXj.vue"
import BjFl from "@/views/PortalManagement/Statute/BjFl.vue"
import Tips from "@/views/PortalManagement/Statute/Tips.vue"
import TipsXj from "@/views/PortalManagement/Statute/TipsXj.vue"
import TipsBj from "@/views/PortalManagement/Statute/TipsBj.vue"
//监督举报
import ReportOnInternet from "@/views/PortalManagement/Supervise/ReportOnInternet/index.vue"
import QRReport from "@/views/PortalManagement/Supervise/QRReport/index.vue"
import InvestigationAndEvaluation from "@/views/PortalManagement/Supervise/InvestigationAndEvaluation/index.vue"
import QuestionnaireCreate from "@/views/PortalManagement/Supervise/InvestigationAndEvaluation/QuestionnaireCreate.vue"
//网上举报，举报须知编辑页面
import KnowBiaJi from "@/views/PortalManagement/Supervise/ReportOnInternet/Tips/KnowBiaJi"
//vuex
import store from '@/store'
export default [{
        name: "PortalManagement",
        path: "PortalManagement",
        component: PortalManagement,
        meta: {
            title: '门户管理',
            auth: true,
            code: "wzgl"
        },
        children: [{
                name: "Archives",
                path: "Archives",
                component: Archives,
                meta: {
                    title: '组织机构',
                    auth: true,
                    code: "wzgl-lzda"
                },
            },
            {
                name: "BuildNewForum",
                path: "BuildNewForum",
                component: BuildNewForum,
                meta: {
                    title: '新建-清风论坛-他山之石-警示教育',
                    auth: true,
                    code: "wzgl-cflt"
                },
            },
            {
                name: "GobackArmed",
                path: "GobackArmed",
                component: GobackArmed,
                meta: {
                    title: '编辑武装自己',
                    auth: true,
                    code: "wzgl-cflt"
                },
            },
            {
                name: "BianJiForum",
                path: "BianJiForum",
                component: BianJiForum,
                meta: {
                    title: '编辑-清风论坛',
                    auth: true,
                    code: "wzgl-cflt"
                },
            },
            {
                name: "GoBackClass",
                path: "GoBackClass",
                component: GoBackClass,
                meta: {
                    title: '亦廉课堂-编辑-即将去掉',
                    auth: true,
                    code: "wzgl-cflt-ylkt"
                },
            },
            {
                name: "BianJiClass",
                path: "BianJiClass",
                component: BianJiClass,
                meta: {
                    title: '亦廉课堂-编辑',
                    auth: true,
                    code: "wzgl-cflt-ylkt"
                },
            },
            {
                name: "Classroom",
                path: "Classroom",
                component: Classroom,
                meta: {
                    title: '亦廉课堂',
                    auth: true,
                    code: "wzgl-cflt-ylkt"
                },
                beforeEnter: (to, from, next) => {
                    // ...
                    if (from.name !== "BianJiClass") {
                        store.commit("PortalManagement/SET_PAGE_SIZE", 10)
                        store.commit("PortalManagement/SET_PAGE_NUM", 1)
                    }
                    next()
                }
            },
            {
                name: "ClassTextEditor",
                path: "ClassTextEditor",
                component: ClassTextEditor,
                meta: {
                    title: '亦廉课堂-新建',
                    auth: true,
                    code: "wzgl-cflt-ylkt"
                },
            },
            {
                name: "KnowBiaJi",
                path: "KnowBiaJi",
                component: KnowBiaJi,
                meta: {
                    title: '监督举报-网上举报-举报须知',
                    auth: true,
                    code: "wzgl-jdjb-wsjb"
                },
            },
            {
                name: "OutsideBox",
                path: "OutsideBox",
                component: OutsideBox,
                meta: {
                    title: '他山之石',
                    auth: true,
                    code: "wzgl-cflt-tszs"
                },
                beforeEnter: (to, from, next) => {
                    // ...
                    if (from.name !== "BianJiForum") {
                        store.commit("PortalManagement/SET_PAGE_SIZE", 10)
                        store.commit("PortalManagement/SET_PAGE_NUM", 1)
                    }
                    next()
                }
            },
            {
                name: "Warning",
                path: "Warning",
                component: Warning,
                meta: {
                    title: '警示教育',
                    auth: true,
                    code: "wzgl-cflt-jsjy"
                },
                beforeEnter: (to, from, next) => {
                    // ...
                    if (from.name !== "BianJiForum") {
                        store.commit("PortalManagement/SET_PAGE_SIZE", 10)
                        store.commit("PortalManagement/SET_PAGE_NUM", 1)
                    }
                    next()
                }
            },
            {
                name: "WarningTextEditor",
                path: "WarningTextEditor",
                component: WarningTextEditor,
                meta: {
                    title: '警示教育-新建-未使用',
                    auth: true,
                    code: "wzgl-cflt-jsjy"
                },
            },
            {
                name: "Home",
                path: "Home",
                component: Home,
                meta: {
                    title: '首页',
                    auth: true,
                    code: "wzgl-sy"
                },
                beforeEnter: (to, from, next) => {
                    // ...
                    if (from.name !== "BianJi") {
                        store.commit("PortalManagement/SET_PAGE_SIZE", 10)
                        store.commit("PortalManagement/SET_PAGE_NUM", 1)
                    }
                    next()
                }
            },
            {
                name: "TextEditor",
                path: "TextEditor",
                component: TextEditor,
                meta: {
                    title: '首页-轮播页新建',
                    auth: true,
                    code: "wzgl-sy"
                },
            },
            {
                name: "GoBack",
                path: "GoBack",
                component: GoBack,
                meta: {
                    title: '首页-轮播页打回编辑-未使用',
                    auth: true,
                    code: "wzgl-sy"
                },
            },
            {
                name: "BianJi",
                path: "BianJi",
                component: BianJi,
                meta: {
                    title: '首页-轮播页-编辑',
                    auth: true,
                    code: "wzgl-sy"
                },
            },
            {
                name: "Inspection",
                path: "Inspection",
                component: Inspection,
                meta: {
                    title: '中央动态',
                    auth: true,
                    code: "wzgl-xsxc-zydt"
                },
            },
            {
                name: "Introduction",
                path: "Introduction",
                component: Introduction,
                meta: {
                    title: '系统简介',
                    auth: true,
                    code: "wzgl-xtjj"
                },
            },
            {
                name: "Investigation",
                path: "Investigation",
                component: Investigation,
                meta: {
                    title: '执纪审查-未使用',
                    auth: true,
                    code: "wzgl-scdc"
                },
            },
            {
                name: "Risk",
                path: "Risk",
                component: Risk,
                meta: {
                    title: '思想道德风险',
                    auth: true,
                    code: "wzgl-fxfk-sxddfx"
                },

            },
            {
                name: "Sx",
                path: "Sx",
                component: Sx,
                meta: {
                    title: '风险防控-未用到',
                    auth: true,
                    code: "wzgl-fxfk"
                },
            },
            {
                name: "BeiJing",
                path: "BeiJing",
                component: BeiJing,
                meta: {
                    title: '北京动态',
                    auth: true,
                    code: "wzgl-xsxc-bjdt"
                },
            },
            {
                name: "BJXJ",
                path: "BJXJ",
                component: BJXJ,
                meta: {
                    title: '北京动态-新建',
                    auth: true,
                    code: "wzgl-xsxc-bjdt"
                },
            },
            {
                name: "BJBJ",
                path: "BJBJ",
                component: BJBJ,
                meta: {
                    title: '北京动态-编辑',
                    auth: true,
                    code: "wzgl-xsxc-bjdt"
                },
            },
            {
                name: "Group",
                path: "Group",
                component: Group,
                meta: {
                    title: '集团动态',
                    auth: true,
                    code: "wzgl-xsxc-jtdt"
                },
            },
            {
                name: "GroupXj",
                path: "GroupXj",
                component: GroupXj,
                meta: {
                    title: '集团动态-新建',
                    auth: true,
                    code: "wzgl-xsxc-jtdt"
                },
            },
            {
                name: "GroupBj",
                path: "GroupBj",
                component: GroupBj,
                meta: {
                    title: '集团动态-编辑',
                    auth: true,
                    code: "wzgl-xsxc-jtdt"
                },
            },
            {
                name: "Special",
                path: "Special",
                component: Special,
                meta: {
                    title: '专题维护',
                    auth: true,
                    code: "wzgl-xsxc-zt"
                },
            },
            {
                name: "SpecialXj",
                path: "SpecialXj",
                component: SpecialXj,
                meta: {
                    title: '专题维护-新建',
                    auth: true,
                    code: "wzgl-xsxc-zt"
                },
            },
            {
                name: "ZH",
                path: "ZH",
                component: ZH,
                meta: {
                    title: '中央动态',
                    auth: true,
                    code: "wzgl-xsxc-zydt"
                },
            },
            {
                name: "ZHBJ",
                path: "ZHBJ",
                component: ZHBJ,
                meta: {
                    title: '中央动态-编辑',
                    auth: true,
                    code: "wzgl-xsxc-zydt"
                },
            },
            {
                name: "SpecialBj",
                path: "SpecialBj",
                component: SpecialBj,
                meta: {
                    title: '专题维护-编辑',
                    auth: true,
                    code: "wzgl-xsxc-zt"
                },
            },
            {
                name: "PD",
                path: "PD",
                component: PD,
                meta: {
                    title: '党纪政务处分',
                    auth: true,
                    code: "wzgl-scdc-djzwcf"
                },
            },
            {
                name: "PDXJ",
                path: "PDXJ",
                component: PDXJ,
                meta: {
                    title: '党纪政务处分-新建',
                    auth: true,
                    code: "wzgl-scdc-djzwcf"
                },
            },
            {
                name: "PDBJ",
                path: "PDBJ",
                component: PDBJ,
                meta: {
                    title: '党纪政务处分-编辑',
                    auth: true,
                    code: "wzgl-scdc-djzwcf"
                },
            },
            {
                name: "SxXj",
                path: "/SxXj",
                component: SxXj,
                meta: {
                    title: '思想道德风险-新建',
                    auth: true,
                    code: "wzgl-fxfk-sxddfx"
                },
            },

            {
                name: "SxBj",
                path: "SxBj",
                component: SxBj,
                meta: {
                    title: '思想道德风险-编辑',
                    auth: true,
                    code: "wzgl-fxfk-sxddfx"
                },
            },
            // 党纪法规
            {
                name: "Statute",
                path: "Statute",
                component: Statute,
                meta: {
                    title: '分类系统',
                    auth: true,
                    code: "wzgl-djfg-wsjb"
                },
            },
            {
                name: "FlXj",
                path: "FlXj",
                component: FlXj,
                meta: {
                    title: '分类系统-新建',
                    auth: true,
                    code: "wzgl-djfg-wsjb"
                },
            },
            {
                name: "BjFl",
                path: "BjFl",
                component: BjFl,
                meta: {
                    title: '分类系统-编辑',
                    auth: true,
                    code: "wzgl-djfg-wsjb"
                },
            },
            {
                name: "Tips",
                path: "Tips",
                component: Tips,
                meta: {
                    title: '小贴士',
                    auth: true,
                    code: "wzgl-djfg-xts"
                },
            },
            {
                name: "TipsXj",
                path: "TipsXj",
                component: TipsXj,
                meta: {
                    title: '小贴士-新建',
                    auth: true,
                    code: "wzgl-djfg-xts"
                },
            },
            {
                name: "TipsBj",
                path: "TipsBj",
                component: TipsBj,
                meta: {
                    title: '小贴士-编辑',
                    auth: true,
                    code: "wzgl-djfg-xts"
                },
            },
            {
                name: "ReportOnInternet",
                path: "ReportOnInternet",
                component: ReportOnInternet,
                meta: {
                    title: '网上举报',
                    auth: true,
                    code: "wzgl-jdjb-wsjb"
                },
            },
            {
                name: "QRReport",
                path: "QRReport",
                component: QRReport,
                meta: {
                    title: '二维码举报',
                    auth: true,
                    code: "wzgl-jdjb-ewmjb"
                },
            },
            {
                name: "InvestigationAndEvaluation",
                path: "InvestigationAndEvaluation",
                component: InvestigationAndEvaluation,
                meta: {
                    title: '调查评估',
                    auth: true,
                    code: "wzgl-jdjb-dcpg"
                },
            },
            {
                name: "QuestionnaireCreate",
                path: "InvestigationAndEvaluation/QuestionnaireCreate",
                component: QuestionnaireCreate,
                meta: {
                    title: '问卷调查新建',
                    auth: true,
                    code: "wzgl-jdjb-dcpg"
                },
            },
        ]
    },

]