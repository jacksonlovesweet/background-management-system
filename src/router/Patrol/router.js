// 巡视巡察
import Patrol from "@/views/Patrol/index.vue"
// 问题清单
import Question from "@/views/Patrol/Question"
import Creat from "@/views/Patrol/Question/Creat"
import QuestionInfo from "@/views/Patrol/Question/QuestionInfo"
import QuestionReport from "@/views/Patrol/Question/QuestionReport"
// 整改汇报
import Abarbeitung  from "@/views/Patrol/Abarbeitung"
import ReportInfo from "@/views/Patrol/Abarbeitung/ReportInfo"


export default [
    {
        name: "Patrol",
        path: "Patrol",
        component: Patrol,
        meta: {
            title: '巡视巡查',
            auth: true,
            code:"xsxc"
        },
        redirect: '/index/Patrol/Question',
        children: [
            {
                path: "Question",
                name: "Question",
                meta: {
                    title: '问题清单',
                    auth: true,
                    code:"xsxc-wtqd_manage",
                    code:"xsxc-wtqd_upload"
                },
                component: Question,
            },
            {
                path: "Question/Creat",
                name: "Creat",
                meta: {
                    auth: true,
                    code:"xsxc-wtqd_manage",
                    code:"xsxc-wtqd_upload"
                },
                component: Creat,
            },
            {
                path: "Question/QuestionReport",
                name: "QuestionReport",
                meta: {
                    auth: true,
                    code:"xsxc-wtqd_manage",
                    code:"xsxc-wtqd_upload"
                },
                component: QuestionReport,
            }, 
            {
                path: "Question/QuestionInfo",
                name: "QuestionInfo",
                meta: {
                    auth: true,
                    code:"xsxc-wtqd_manage",
                    code:"xsxc-wtqd_upload"
                },
                component: QuestionInfo,
            },
            {
                path: "Abarbeitung",
                name: "Abarbeitung",
                meta: {
                    title: '整改汇报',
                    auth: true,
                    code:"xsxc-zghb"
                },
                component: Abarbeitung,
            },
            {
                path: "Abarbeitung/ReportInfo",
                name: "ReportInfo",
                meta: {
                    auth: true,
                    code:"xsxc-zghb"
                },
                component: ReportInfo,
            },
        ]
    },
]