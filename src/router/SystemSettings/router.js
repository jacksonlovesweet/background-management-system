// 系统设置
import SystemSettings from "@/views/SystemSettings/index.vue"
// 组织机构
import Organization from "@/views/SystemSettings/Organization/index.vue"
// 角色管理
import Role from "@/views/SystemSettings/Role/index.vue"
// 用户管理
import User from "@/views/SystemSettings/User/index.vue"
// 个人中心
import Personage from "@/views/SystemSettings/Personage/index.vue"
// 字典管理
import Dictionaries from "@/views/SystemSettings/Dictionaries/index.vue"
// 阳光公正
import Sun from "@/views/SystemSettings/Sun/index.vue"
export default [
    {
        name: "SystemSettings",
        path: "SystemSettings",
        redirect:{name:"Organization"},
        component: SystemSettings,
        meta: {
            title: '系统设置',
            auth: true,
            code: "xtsz",
        },
        children: [
            {
                name: "Organization",
                path: "Organization",
                component: Organization,
                meta: {
                    title: '组织机构',
                    auth: true,
                    code: "xtsz-zzjg",
                },
            },
            {
                name: "Role",
                path: "Role",
                component: Role,
                meta: {
                    title: '角色管理',
                    auth: true,
                    code: "xtsz-jsgl",
                },
            },
            {
                name: "User",
                path: "User",
                component:User,
                meta: {
                    title: '用户管理',
                    auth: true,
                    code: "xtsz-yhgl",
                },
            },
            {
                name: "Personage",
                path: "Personage",
                component: Personage,
                meta: {
                    title: '个人中心',
                    auth: true,
                    code: "important_grzx",
                },
            },
            {
                name: "Dictionaries",
                path: "Dictionaries",
                component: Dictionaries,
                meta: {
                    title: '字典管理',
                    auth: true,
                    code: "xtsz-zdgl",
                },
            },
            {
                name: "Sun",
                path: "Sun",
                component: Sun,
                meta: {
                    title: '阳光公正',
                    auth: true,
                    code: "important_grzx",
                },
            },
        ]
    },
]