import welcomePage from "@/views/WelcomePage/welcomePage.vue"
export default [
    {
        name:"welcomePage",
        path:"welcomePage",
        component:welcomePage,
        meta: {
            title: '欢迎页',
            auth: true,
            code: "important_grzx",
        },
    }
]