import Vue from 'vue'
import VueRouter from 'vue-router'
import Index from "@/views/index.vue"
import login from '@/views/Login.vue'
//vuex
import store from '@/store'
//递归函数的获取
// import utils from '@/utils/utils/index.js'
Vue.use(VueRouter)

let routes = [{
        path: '/index',
        name: 'index',
        component: Index,
        redirect: { name: "welcomePage" },
        children: []
    },
    {
        path: "/",
        redirect: {
            name: "login"
        }
    },
    {
        path: "/login",
        name: "login",
        component: login
    }
];

(modules => modules.keys().forEach((key) => {
    routes[0].children = routes[0].children.concat(modules(key).default);
}))(require.context('@/router/', true, /router\.js$/));
const vueRouter = new VueRouter({ routes: routes, mode: "history" });



//所有的路由配置项,拿到权限相关的所有的路由配置项
// let allRoutes = [];
// let allAtth = [];
// let userRoutes = [];
// (modules => modules.keys().forEach((key) => {
//   allRoutes = allRoutes.concat(modules(key).default);
// }))(require.context('@/router/', true, /router\.js$/));
// console.log(allRoutes, "allRoutes完整的路由配置项")



// const sessionRoutesStr = sessionStorage.getItem("routes") 
// const sessionRoutes = sessionRoutesStr && JSON.parse(sessionRoutesStr)

// const vueRouter = new VueRouter({ routes:routes, mode: "history" });
// if(sessionRoutes){

//   vueRouter.matcher = new VueRouter({
//     routes: sessionRoutes,
//     mode: "history"
//   }).matcher
//   console.log(sessionRoutes,"sessionRoutes");
//   console.log(vueRouter,"vueRoutervueRouter");
// }

// //调用获取vuex的数据
// export  function getVuexDta(AllRoutes) {
//   return new Promise( async(resolve) => {
//     await store.dispatch('PortalManagement/SET_PERMISSION_CODE');
//     allAtth = store.state.PortalManagement.PermissionCode;
//     sessionStorage.setItem("allAtth",JSON.stringify(allAtth))
//     allAtth.push("important_grzx");
//     // console.log(allAtth,"allAtthVuex的菜单数据")
//     //数据准备好之后调用递归函数返回需要的路由配置项
//     userRoutes = utils.recursionRoute(allRoutes, allAtth);
//     console.log(userRoutes, "userRoutes获取到的真实的路由")
//     routes[0].children = userRoutes
//     sessionStorage.setItem("routes",JSON.stringify(routes))
//     vueRouter.matcher = new VueRouter({
//       routes: routes,
//       mode: "history"
//     }).matcher
//     console.log(vueRouter, "vueRouter");
//     resolve();
//   })

// }
// getVuexDta(allRoutes)
// console.log(routes, "routes路由全部")


vueRouter.beforeEach(async(to, from, next) => {
        const loginToken = localStorage.getItem("LoginToken");
        const { token } = to.query
        if (token) {
            localStorage.setItem("LoginToken", token);
            next({
                name: to.name
            });
            return
        }
        if (to.name != "login") {
            await store.dispatch('PortalManagement/SET_PERMISSION_CODE');
            //获取到菜单的权限列表-vuex的获取
            let allAtth = store.state.PortalManagement.PermissionCode;
            allAtth.push("important_grzx");
            // console.log(to.meta.code, "to.meta.code");
            if (new Set(allAtth).has(to.meta.code)) {
                next()
                console.log("能访问");
            } else {
                next({ name: login })
                console.log("不能访问");
            }
        }
        next()
    })
    // 双击报错
const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err);
};


export default vueRouter;