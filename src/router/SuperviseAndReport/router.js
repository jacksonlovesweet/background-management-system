import store from '@/store'
// 监管举报
const SuperviseAndReport = resolve => require(['@/views/SuperviseAndReport'], resolve)

// 动态监督
const Expenditure = resolve => require(['@/views/SuperviseAndReport/DynamicMonitoring/Expenditure'], resolve) // 三公经费
const Conference = resolve => require(['@/views/SuperviseAndReport/DynamicMonitoring/Conference'], resolve) // 三重一大
const ConferenceCreate = resolve => require(['@/views/SuperviseAndReport/DynamicMonitoring/Conference/ConferenceCreate'], resolve) // 三重一大上传
const Finance = resolve => require(['@/views/SuperviseAndReport/DynamicMonitoring/Finance'], resolve) // 财务监督
const FinanceTable = resolve => require(['@/views/SuperviseAndReport/DynamicMonitoring/Finance/FinanceTable'], resolve) // 财务监督
    // 专项监督
const Task = resolve => require(['@/views/SuperviseAndReport/SpecialSupervision/Task'], resolve) // 任务管理

const CreatTask = resolve => require(['@/views/SuperviseAndReport/SpecialSupervision/Task/taskCreation'], resolve) // 创建任务
const CreatReport = resolve => require(['@/views/SuperviseAndReport/SpecialSupervision/Task/reportCreation'], resolve) // 创建报告
const MissionDetails = resolve => require(['@/views/SuperviseAndReport/SpecialSupervision/Task/missionDetails'], resolve) // 任务详情
const DetailContainer = resolve => require(['@/views/SuperviseAndReport/SpecialSupervision/Task/DetailContainer'], resolve) // 任务详情
const DetailsOne = resolve => require(['@/views/SuperviseAndReport/SpecialSupervision/Task/detailsOne'], resolve) // 任务详情单个

const Report = resolve => require(['@/views/SuperviseAndReport/SpecialSupervision/Report'], resolve) // 情况报告
const ReportInfo = resolve => require(['@/views/SuperviseAndReport/SpecialSupervision/Report/reportInfo'], resolve) // 情况报告详情
    // 日常监督
const Notification = resolve => require(['@/views/SuperviseAndReport/Daily/Notification'], resolve) // 情况通报
const NotificationCreat = resolve => require(['@/views/SuperviseAndReport/Daily/Notification/notificationCreat'], resolve) // 情况通报创建
const DetailContaner = resolve => require(['@/views/SuperviseAndReport/Daily/Notification/detailContaner'], resolve) // 情况通报详情通用组件

const NotificationReportCreat = resolve => require(['@/views/SuperviseAndReport/Daily/Notification/reportCreat'], resolve) // 情况汇报创建 上报端

const NotificationDetail = resolve => require(['@/views/SuperviseAndReport/Daily/NotificationDetail'], resolve) // 情况通报详情
const DetailOne = resolve => require(['@/views/SuperviseAndReport/Daily/NotificationDetail/detailOne'], resolve) // 情况通报详情单个

const Neaten = resolve => require(['@/views/SuperviseAndReport/Daily/Neaten'], resolve) // 整理汇报
const NeatenDetailOne = resolve => require(['@/views/SuperviseAndReport/Daily/Neaten/detailOne'], resolve) // 整理汇报详情

// 信访举报
const PetitionReport = resolve => require(['@/views/SuperviseAndReport/PetitionReport'], resolve)
    // 请示报告
const Instructions = resolve => require(['@/views/SuperviseAndReport/Instructions'], resolve)
    // 请示报告详情
const InstructionsDetail = resolve => require(['@/views/SuperviseAndReport/Instructions/InstructionsDetail'], resolve)
    //创建请示报告
const CreateInstructions = resolve => require(['@/views/SuperviseAndReport/Instructions/CreateInstructions'], resolve)
    //创建信访举报
const DealPetition = resolve => require(['@/views/SuperviseAndReport/PetitionReport/DealPetition'], resolve)
    //详情信访举报
const PetitionReportDetail = resolve => require(['@/views/SuperviseAndReport/PetitionReport/PetitionReportDetail'], resolve)

export default [{
    path: "SuperviseAndReport",
    name: "SuperviseAndReport",
    meta: {
        title: '监督举报',
        auth: true,
        code: "jdjb"
    },
    component: SuperviseAndReport,
    redirect: '/index/SuperviseAndReport/Expenditure',
    children: [{
            path: "Expenditure",
            name: "Expenditure",
            meta: {
                title: '三共经费',
                auth: true,
                code: "jdjb-dtjd-sgxf"
            },
            component: Expenditure,
        },
        {
            path: "Conference",
            name: "Conference",
            meta: {
                title: '三重一大',
                auth: true,
                code: "jdjb-dtjd-szyd_manage",

            },
            component: Conference,
        },
        {
            path: "Conference/ConferenceCreate",
            name: "ConferenceCreate",
            meta: {
                title: '三重一大上传',
                auth: true,
                code: "jdjb-dtjd-szyd_manage"
            },
            component: ConferenceCreate,
        },
        {
            path: "Finance",
            name: "Finance",
            meta: {
                title: '财务监督',
                auth: true,
                code: "jdjb-dtjd-cwjd"
            },
            component: Finance,
        },
        {
            path: "Finance/FinanceTable",
            name: "FinanceTable",
            meta: {
                title: '财务监督表格',
                auth: true,
                code: "jdjb-dtjd-cwjd"
            },
            component: FinanceTable,
        },
        {
            path: "Task",
            name: "Task",
            meta: {
                title: '任务管理',
                auth: true,
                code: "important_grzx"
            },
            component: Task,
        },
        {
            path: "Task/CreatTask",
            name: "CreatTask",
            meta: {
                title: '创建任务',
                auth: true,
                code: "important_grzx"
            },
            component: CreatTask,
        },
        {
            path: "Task/CreatReport",
            name: "CreatReport",
            meta: {
                title: '创建汇报',
                auth: true,
                code: "important_grzx"
            },
            component: CreatReport,
        },
        {
            path: "Task/MissionDetails",
            name: "MissionDetails",
            meta: {
                title: '任务详情',
                auth: true,
                code: "important_grzx"
            },
            component: MissionDetails,
        },
        {
            path: "Task/DetailContainer",
            name: "DetailContainer",
            meta: {
                title: '任务详情',
                auth: true,
                code: "important_grzx"
            },
            component: DetailContainer,
        },
        {
            path: "Task/DetailsOne",
            name: "DetailsOne",
            meta: {
                title: '任务详情单个',
                auth: true,
                code: "important_grzx"
            },
            component: DetailsOne,
        },

        {
            path: "Report",
            name: "Report",
            meta: {
                title: '情况报告',
                auth: true,
                code: "important_grzx"
            },
            component: Report,
        },
        {
            path: "Report/ReportInfo",
            name: "ReportInfo",
            meta: {
                title: '情况报告详情',
                auth: true,
                code: "important_grzx"
            },
            component: ReportInfo,
        },

        {
            path: "Notification",
            name: "Notification",
            meta: {
                title: '情况通报',
                auth: true,
                code: "jdjb-rcjd-qktb_manage"
            },
            component: Notification,
        },
        {
            path: "Notification/NotificationCreat",
            name: "NotificationCreat",
            meta: {
                title: '新建情况通报',
                auth: true,
                code: "jdjb-rcjd-qktb_manage"
            },
            component: NotificationCreat,
        },
        {
            path: "Notification/DetailContaner",
            name: "DetailContaner",
            meta: {
                title: '情况通报详情',
                auth: true,
                code: "jdjb-rcjd-qktb_manage"
            },
            component: DetailContaner,
        },
        {
            path: "Notification/NotificationReportCreat",
            name: "NotificationReportCreat",
            meta: {
                title: '新建汇报（上报端）',
                auth: true,
                code: "jdjb-rcjd-qktb_manage"
            },
            component: NotificationReportCreat,
        },

        {
            path: "NotificationDetail",
            name: "NotificationDetail",
            meta: {
                title: '情况通报详情',
                auth: true,
                code: "jdjb-rcjd-qktb_manage"
            },
            component: NotificationDetail,
        },
        {
            path: "NotificationDetail/DetailOne",
            name: "DetailOne",
            meta: {
                title: '情况通报单个详情',
                auth: true,
                code: "jdjb-rcjd-qktb_manage"
            },
            component: DetailOne,
        },



        {
            path: "Neaten",
            name: "Neaten",
            meta: {
                title: '整改汇报',
                auth: true,
                code: "jdjb-rcjd-zghb"
            },
            component: Neaten,
        },
        {
            path: "Neaten/NeatenDetailOne",
            name: "NeatenDetailOne",
            meta: {
                title: '整改汇报详情',
                auth: true,
                code: "jdjb-rcjd-zghb"
            },
            component: NeatenDetailOne,
        },

        {
            path: "PetitionReport",
            name: "PetitionReport",
            meta: {
                title: '信访举报',
                auth: true,
                code: "jdjb-xfjb"
            },
            component: PetitionReport
        },
        {
            path: "PetitionReport/PetitionReportDetail",
            name: "PetitionReportDetail",
            meta: {
                title: '信访举报详情',
                auth: true,
                code: "jdjb-xfjb"
            },
            component: PetitionReportDetail
        },
        {
            path: "PetitionReport/DealPetition",
            name: "DealPetition",
            meta: {
                title: '受理信访举报',
                auth: true,
                code: "jdjb-xfjb"
            },
            component: DealPetition
        },
        {
            path: "Instructions",
            name: "Instructions",
            meta: {
                title: '请示报告',
                auth: true,
                code: "important_grzx"
            },
            component: Instructions
        },
        {
            path: "Instructions/InstructionsDetail",
            name: "InstructionsDetail",
            meta: {
                title: '请示报告详情',
                auth: true,
                code: "important_grzx"
            },
            component: InstructionsDetail
        },
        {
            path: "Instructions/CreateInstructions",
            name: "CreateInstructions",
            meta: {
                title: '创建请示报告',
                auth: true,
                code: "important_grzx"
            },
            component: CreateInstructions
        },
    ]
}, ]