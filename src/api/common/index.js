import {
  $get,
  $post
} from '@/utils/http'

export default {
  // 获取图片基础路径
  getIp() {
    return $post('/common/serverUrl')
  },
  // 审批意见接口
  getThing(params) {
    return $post('/admin/Opinion/opinion/listOpinion', params)
  },
  //字典接口字典项列表
  getDictionaries(params) {
    return $post('/admin/sys/dict/item-list', params)
  },
  //图片上传接口
  uploadImage(params) {
    return $post('/common/image-upload', params)
  },
  //文件上传接口
  uploadFile(params) {
    return $post('/common/file-upload', params)
  },
  //文件下载接口
  download(params) {
    return $post('/common/download?fileUrl='+ params,null,null,{responseType:'blob'})
  },
  //权限菜单的接口
  PermissionCode(params) {
    return $post('/admin/sys/self/authority', params)
  },
  //字典相关的下拉列表
  getDictionariesDropDown(params){
    return $post('/admin/sys/dict/type-item-list', params)
  }
}


