import {
    $get,
    $post
} from '@/utils/http'

export default {

    //  问题清单列表（管理端）
    questionList(params) {

        return $post('/admin/inspect/problem/manage/page', params)

    },
    // 整改对象下拉
    questionSelet(params) {

        return $get('/admin/inspect/problem/manage/org-list', params)
    },
    // 创建问题单
    createdQuestion(params) {
        return $post('/admin/inspect/problem/manage/add', params)

    },
    // 问题详情
    detailQuestion(params) {
        return $post('/admin/inspect/problem/manage/detail', params)
    },
    // 整改过程汇报内容
    textQuestion(params) {
        return $post('/admin/inspect/problem/manage/detail/report-content', params)
    },
    //  问题清单列表（上传端）
    questionList1(params) {

        return $post('/admin/inspect/problem/upload/page', params)

    },
    // 问题详情
    uploadDetail(params) {
        return $post('/admin/inspect/problem/upload/detail', params)
    },
    // 问题单汇报回显
    questtionEcho(params) {
        return $post('/admin/inspect/problem/upload/report/echo', params)
    },
    addQuestion(params) {
        return $post('/admin/inspect/problem/upload/report/add', params)
    },
    // 整改过程汇报详情
    detailReport(params) {
        return $post('/admin/inspect/problem/upload/report/detail', params)
    },
    // 编辑整改过程汇报
    editReport(params) {
        return $post('/admin/inspect/problem/upload/report/edit', params)
    },
    // 删除整改过程
    deleteReport(params) {
        return $post('/admin/inspect/problem/upload/report/delete', params)
    },
    submitReport(params) {
        return $post('/admin/inspect/problem/upload/report/submit', params)
    },
    // 整改汇报
    // 整改汇报分页列表
    rectifyPage(params) {
        return $post('/admin/inspect/rectify/page', params)
    },
    // 整改汇报详情
    rectifyDetail(params) {
        return $post('/admin/inspect/rectify/detail', params)
    },
    // 整改汇报内容
    reportContent(params) {
        return $post('/admin/inspect/rectify/detail/report-content', params)
    },
    // 下载
    downLoad(params) {
        return $post('/admin/inspect/rectify/download', params)
    },
    // 催办
    cuiban(params) {
        return $post('/admin/inspect/problem/manage/urging', params)
    },

}