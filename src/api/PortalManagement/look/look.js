import {
    $get,
    $post
} from '@/utils/http'
//巡视巡察
export default {
    // 中央动态
    //  中央动态分类列表
    getCenter(params) {

        return $post('/admin/websit/centralInfo/listCentralPatrol', params)

    },

    // 删除
    centerDelete(params) {
        return $post('/admin/websit/centralInfo/delCentralPatrol', params)
    },
    // 动态删除
    centerDeleteOne(params) {
        return $post('/admin/websit/centralEx/delExById', params)
    },
    // 上移
    lookMove(params) {

        return $post('/admin/websit/centralInfo/centralUp', params)

    },
    // 下移
    lookMove1(params) {

        return $post('/admin/websit/centralInfo/centralDown', params)

    },
    // 上驾
    lookUp(params) {

        return $post('/admin/websit/centralInfo/groundingCentral', params)

    },
    // 下驾
    lookDown(params) {

        return $post('/admin/websit/centralInfo/withdrawCentral', params)

    },

    //    搜索
    searchCenter1(params) {

        return $post('/admin/websit/centralInfo/listCentralPatrol', params)

    },
    // 新增
    newCenter(params) {
        return $post('/admin/websit/centralInfo/saveCentralPatrol', params)
    },
    // 编辑详情
    EditCenter(params) {
        return $post('/admin/websit/centralInfo/getCentralPatrolById', params)
    },
    // 中央新建分类获取
    getClassify(params) {
        return $get('/common/dict/item-list', params)
    },
    // 北京

    // 北京分类列表
    getBeijing(params) {

        return $post('/admin/websit/beijingInfo/listPatrolInfo', params)

    },

    // 上架北京
    BjUp(params) {

        return $post('/admin/websit/beijingInfo/groundingBj', params)

    },
    //   下架北京
    BjDown(params) {

        return $post('/admin/websit/beijingInfo/withdrawBj', params)

    },
    // 搜索北京
    BjSearch(params) {

        return $post('/admin/websit/beijingInfo/listPatrolInfo', params)

    },
    // 删除
    BjDelete(params) {

        return $post('/admin/websit/beijingInfo/delPatrolInfo', params)

    },
    // 北京新建
    BjNew(params) {

        return $post('/admin/websit/beijingInfo/savePatrolInfo', params)

    },
    // 北京详情
    BjEdit(params) {

        return $post('/admin/websit/beijingInfo/getPatrolInfoById', params)

    },
    // 北京分类下拉
    BjFl(params) {

        return $post('/admin/websit/beijingInfo/getPatrolInfoById', params)

    },
    // 分类
    Fenlei(params) {
        return $get('/common/dict/item-list', params)  
    },
    // 分类
    Fenlei1(params) {
        return $get('/common/dict/item-list', params)  
    },
    Fenlei2(params) {
        return $get('/common/dict/item-list', params)  
    },
    Fenlei3(params) {
        return $get('/common/dict/item-list', params)  
    },
    //集团
    // 集团分类列表
    getGroup(params) {

        return $post('/admin/websit/groupInfo/listPatrolInfo', params)

    },
    // 所属专题
    getSpecial(params) {

        return $post('/admin/websit/subject/listAllSubject', params)

    },
    //集团搜索
    groupSearch(params) {

        return $post('/admin/websit/groupInfo/listPatrolInfo', params)

    },
    // 集团新建
    groupNew(params) {

        return $post('/admin/websit/groupInfo/addPatrolInfo', params)

    },
    // 集团编辑
    groupEdit(params) {

        return $post('/admin/websit/groupInfo/getPatrolInfoById', params)

    },
    // 集团编辑
    groupEdit1(params) {

        return $post('/admin/websit/groupInfo/editPatrolInfo', params)

    },
     // 集团删除
     groupDelete(params) {

        return $post('/admin/websit/groupInfo/delPatrolInfo', params)

    },
    //  审核
    reviewAudit(params) {
        return $post('/admin/websit/groupInfo/auditPatrolInfo', params)
    },
    //  审核意见
    reviewAudit1(params) {
        return $post('/admin/Opinion/opinion/listOpinion', params)
    },
    //  发布
    reviewPublish(params) {
        return $post('/admin/websit/groupInfo/publishPatrolInfo', params)
    },
    // 专题维护

    // 获取专题维护列表
    getSpecial(params) {
        return $post('/admin/websit/subject/listAllSubject', params)
    },
    // 专题上架
    specialUp(params) {
        return $post('/admin/websit/subject/groundingSubject', params)
    },
    // 专题下架
    specialDown(params) {
        return $post('/admin/websit/subject/withdrawSubject', params)
    },
    // 专题上移
    specialMove(params) {
        return $post('/admin/websit/subject/subjectUp', params)
    },
    // 专题下移
    specialMove1(params) {
        return $post('/admin/websit/subject/subjectDown', params)
    },
    // 专题查询
    specialSearch(params) {
        return $post('/admin/websit/subject/listSubject', params)
    },
    // 专题删除
    specialDelete(params) {
        return $post('/admin/websit/subject/delSubject', params)
    },
    // 专题新建
    specialNew(params) {
        return $post('/admin/websit/subject/addSubject', params)
    },
    // 专题编辑
    specialEdit(params) {
        return $post('/admin/websit/subject/getSubjectById', params)
    },
    // 专题编辑提交
    specialEdit1(params) {
        return $post('/admin/websit/subject/editSubject', params)
    },
}