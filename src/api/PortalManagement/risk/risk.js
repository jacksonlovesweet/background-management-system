import {
    $get,
    $post
} from '@/utils/http'
// 风险防控
export default {
    // 思想道德风险列表
    getBisk1(params) {

        return $post('/admin/review/moralityRisk/listReview', params)

    },
    // 上架思想道德风险
    riskUp(params) {

        return $post('/admin/review/moralityRisk/groundingReview', params)

    },
    // 下架思想道德风险
    riskDown(params) {

        return $post('/admin/review/moralityRisk/withdrawReview', params)

    },
    //  搜索
    riskSearch(params) {

        return $post('/admin/review/moralityRisk/listReview', params)

    },
    // 删除
    riskDelete(params) {
        return $post('/admin/review/moralityRisk/delReview', params)  
    },
    // 新建
    riskNew(params) {
        return $post('/admin/review/moralityRisk/addReview', params)  
    },
    //查询思想道德风险信息详情
    riskEdit(params) {
        return $post('/admin/review/moralityRisk/getReviewById', params)  
    },
    // 提交
    riskEdit1(params) {
        return $post('/admin/review/moralityRisk/editReview', params)  
    },  
}