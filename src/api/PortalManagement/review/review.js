import {
    $get,
    $post
} from '@/utils/http'
//审查调查
export default {
    // 党纪处分
    //  党纪处分分类列表
    getReview(params) {

        return $post('/admin/review/punishmentReview/listReview', params)

    },
       //搜索
    reviewDelete(params) {
        return $post('/admin/review/punishmentReview/listReview', params)  
    },
     
 //新建
 reviewNew(params) {
    return $post('/admin/review/punishmentReview/addReview', params)  
},
  //编辑
  reviewEdit(params) {
    return $post('/admin/review/punishmentReview/getReviewById', params)  
},
  //提交
  reviewEdit1(params) {
    return $post('/admin/review/punishmentReview/editReview', params)  
},
//  审核
 reviewAudit1(params) {
    return $post('/admin/review/punishmentReview/auditReview', params)  
},
//  审核意见
reviewAudit2(params) {
    return $post('/admin/Opinion/opinion/listOpinion', params)  
},
//  发布
reviewPublish3(params) {
    return $post('/admin/review/punishmentReview/publishReview', params)  
},
}