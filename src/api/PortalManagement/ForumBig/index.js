import {
    $get,
    $post
} from '@/utils/http'
// 清风论坛
export default {
    //亦廉课堂列表
    getForumClassList(params) {
        return $post('/admin/forum/ylClassRoom/listArticle', params)
    },
    //亦廉课堂新增加
    getForumClassAdd(params) {
        return $post('/admin/forum/ylClassRoom/addArticle', params)
    },
    //查询亦廉课堂单条信息
    getForumClassEach(params) {
        return $post('/admin/forum/ylClassRoom/getArticleById', params)
    },
    //查询亦廉课堂单条信息
    getForumClasschoise(params) {
        return $post('/admin/forum/ylClassRoom/auditArticle', params)
    },
    //查询亦廉课堂修改数据
    getForumClassChange(params) {
        return $post('/admin/forum/ylClassRoom/editArticle', params)
    },
    //查询亦廉课堂发布接口
    getForumClassSend(params) {
        return $post('/admin/forum/ylClassRoom/publishReview', params)
    },
    //他山之石
    //学习他人列表
    getStudyList(params) {
        return $post('/admin/forum/learningOthers/listArticle', params)
    },
    //学习他人上架
    getStudyUp(params) {
        return $post('/admin/forum/learningOthers/groundingArticle', params)
    },
    //学习他人下架
    getStudyDown(params) {
        return $post('/admin/forum/learningOthers/withdrawArticle', params)
    },
    //学习他人删除接口
    getStudyClear(params) {
        return $post('/admin/forum/learningOthers/delArticle', params)
    },
    //学习他人修改接口
    getStudyChange(params) {
        return $post('/admin/forum/learningOthers/editArticle', params)
    },
    //学习他人新建
    getStudyBuild(params) {
        return $post('/admin/forum/learningOthers/addArticle', params)
    },
    //警示教育列表
    getWarningList(params) {
        return $post('/admin/forum/warning/listArticle', params)
    },
    //警示教育上架
    getWarningUp(params) {
        return $post('/admin/forum/warning/groundingArticle', params)
    },
    //警示教育xia架
    getWarningDown(params) {
        return $post('/admin/forum/warning/withdrawArticle', params)
    },
    //警示教育新建
    getWarningBuild(params) {
        return $post('/admin/forum/warning/addArticle', params)
    },
    //警示教删除
    getWarningClear(params) {
        return $post('/admin/forum/warning/delArticle', params)
    },
    //警示编辑
    getWarningBianji(params) {
        return $post('/admin/forum/warning/editArticle', params)
    },
    //他山之石、、武装自己
    getArmedList(params) {
        return $post('/admin/forum/armYouSelf/listArticle', params)
    },
     //他山之石、、武装自己-新建
     getArmedAdd(params) {
        return $post('/admin/forum/armYouSelf/addArticle', params)
    },
    //他山之石、、武装自己-审批接口
    getArmedChoise(params) {
        return $post('/admin/forum/armYouSelf/auditArticle', params)
    },
    //他山之石、、武装自己-单个详情
    getArmederery(params) {
        return $post('/admin/forum/armYouSelf/getArticleById', params)
    },
    //他山之石、、武装自己-发布
    getArmedSend(params) {
        return $post('/admin/forum/armYouSelf/publishArticle', params)
    },
    //他山之石、、武装自己-删除
    getArmedClear(params) {
        return $post('/admin/forum/armYouSelf/delArticle', params)
    },
     //警示教育，编辑格列数据详情
     geteducationDetail(params) {
        return $post('/admin/forum/warning/getArticleById', params)
    },
    //xuexi他人，编辑格列数据详情
    getoutsideDetail(params) {
        return $post('/admin/forum/learningOthers/getArticleById', params)
    },
    
}




