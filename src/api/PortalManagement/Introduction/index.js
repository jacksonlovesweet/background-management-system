import {
    $get,
    $post
} from '@/utils/http'
// 系统简介接口
export default {
    //系统简介接口获取列表
    getBriefList() {
        return $post('/admin/websitadmin/websiteIntroduction/listIntroduction')
    },
    //系统简介接口修改保存的接口
    getBriefSavue(params) {
        return $post('/admin/websitadmin/websiteIntroduction/editIntroduction', params)
    },
    //系统简介删除的接口
    getBriefRemove(params) {
        return $post('/admin/websitadmin/websiteIntroduction/delIntroduction', params)
    },
    //系统简介删除的接口
    getBriefRemove(params) {
        return $post('/admin/websitadmin/websiteIntroduction/delIntroduction', params)
    },
    //系统简介增加单条数据的接口
    getBriefSavueNew(params) {
        return $post('/admin/websitadmin/websiteIntroduction/addIntroduction', params)
    },




}




