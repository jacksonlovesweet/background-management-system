import {
    $get,
    $post
} from '@/utils/http'
// 监督举报接口
export default {
    //问卷调查
    // 查询问卷调查信息列表
    getAssessmentListAssessment(data) {
        return $post('/admin/websit/assessment/listAssessment', data)
    },
    // 问卷调查信息列表内提交
    getAssessmentListSubmitAssessment(data) {
        return $post('/admin/websit/assessment/submitAssessment', data)
    },
    // 问卷调查信息列表内删除
    getAssessmentListDelAssessment(data) {
        return $post('/admin/websit/assessment/delAssessment', data)
    },
    // 问卷调查信息列表内发布
    getAssessmentListPublishAssessment(data) {
        return $post('/admin/websit/assessment/publishAssessment', data)
    },
    // 查询问卷调查二维码
    getAssessmentListGetRqcodeById(data) {
        return $post('/admin/websit/assessment/getRqcodeById', data)
    },
    // 增加单条问卷调查
    getAssessmentListAddAssessment(data) {
        return $post('/admin/websit/assessment/addAssessment', data)
    },

    // 工作评估
    // 查询工作评估列表
    getJobAssessmentListJobAssessment(data) {
        return $post('/admin/websit/jobAssessment/listJobAssessment', data)
    },


    //题库管理 
    getQuestionBankListQuestion(data) {
        return $post('/admin/websit/questionBank/listQuestion', data)
    },
}