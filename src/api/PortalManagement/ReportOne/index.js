import {
    $get,
    $post
} from '@/utils/http'
// 监督举报接口
export default {
    //网上举报图片获取,举报须知，二维码举报共用一个接口getReportAll
    getReportAll() {
        return $post('/admin/websit/complainSeting/getComplainSeting')
    },
    //网上举报图片保存及举报须知编辑后保存公用一个接口
    getReportSaveAll(params) {
        return $post('/admin/websit/complainSeting/saveComplainOnLine', params)
    },
    //网上举报相关法律接口
    getReportLaw(params) {
        return $post('/admin/websit/complainSeting/listAllComplainLaw', params)
    },
    //网上举报法律上移
    getReportLawUp(params) {
        return $post('/admin/websit/complainSeting/moveUp', params)
    },
    //网上举报法律下移
    getReportLawDown(params) {
        return $post('/admin/websit/complainSeting/moveDown', params)
    },
    //网上举报法律删除接口
    getReportLawRemove(params) {
        return $post('/admin/websit/complainSeting/delComplainLaw', params)
    },
    //弹窗法律法规列表
    getReportLawList(params) {
        return $post('/admin/websit/complainSeting/listClassification', params)
    },
    //保存图片
    buildPic(params) {
        return $post('/admin/websit/complainSeting/saveComplainOnLine', params)

    },
    //保存二维码举报
    BlobBuilder(params) {
        return $post('/admin/websit/complainSeting/saveComplainRqCode', params)
    },
    //网上举报弹窗提交
    BuilobSubmit(params) {
        return $post('/admin/websit/complainSeting/addComplainLaw', params)
    }

}




