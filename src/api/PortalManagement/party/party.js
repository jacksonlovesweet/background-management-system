import {
    $get,
    $post
} from '@/utils/http'
// 党纪法规
export default {
    // 分类列表
    get1Classify(params) {

        return $post('/admin/websit/classificationSystem/listClassificationSystemInfo', params)

    },
//    
       // 删除
    partyDelete(params) {
           console.log(123);
        return $post('/admin/websit/classificationSystem/delClassificationSystemInfo', params)  
    },
        // 上架分类
    partyUp(params) {

        return $post('/admin/websit/classificationSystem/groundingClassificationSystemInfo', params)

    },
    // 下架分类
   partyDown(params) {

        return $post('/admin/websit/classificationSystem/withdrawClassificationSystemInfo', params)

    },
//    搜索
searchClassify(params) {

    return $post('/admin/websit/classificationSystem/listClassificationSystemInfo', params)

    },
    // 新建
    partyNew(params) {
        return $post('/admin/websit/classificationSystem/addClassificationSystemInfo', params)  
    },
    //查询思想道德风险信息详情
    partyEdit(params) {
        return $post('/admin/websit/classificationSystem/getClassificationSystemInfoById', params)  
    },
    // 提交
    partyEdit1(params) {
        return $post('/admin/websit/classificationSystem/editClassificationSystemInfo', params)  
    },  
    // 分类
    Fenlei(params) {
        return $post('/admin/sys/dict/item-list', params)  
    },
    // 分类
    Fenlei1(params) {
        return $post('/admin/sys/dict/item-list', params)  
    },
   
// 小贴士
// 小贴士列表
  // 分类列表
  getTips(params) {

    return $post('/admin/websit/tipsInfo/listTip', params)

    },
//   上架小贴士
   // 上架分类
   tipsUp(params) {

    return $post('/admin/websit/tipsInfo/groundingTip', params)

},
//   下架小贴士
tipsDown(params) {

    return $post('/admin/websit/tipsInfo/withdrawTip', params)

}, 
// 查询小贴士
tipsSearch(params) {

    return $post('/admin/websit/tipsInfo/listTip', params)

    },
    // 删除
    tipsDelete(params) {

        return $post('/admin/websit/tipsInfo/delTip', params)
    
    },
     // 新建
     tipsNew(params) {
        return $post('/admin/websit/tipsInfo/addTip', params)  
    },
    //查询思想道德风险信息详情
    tipsEdit(params) {
        return $post('/admin/websit/tipsInfo/getTipById', params)  
    },
    // 提交
    tipsEdit1(params) {
        return $post('/admin/websit/tipsInfo/editTip', params)  
    },  
}