import {
    $get,
    $post
} from '@/utils/http'
// 廉政档案接口
export default {
    //廉政档案接口获取列表-领导机构
    getDanganLeadingList() {
        return $post('/admin/websit/incorruptibleRecordManager/selectAllByLD')
    },
    //领导机构修改数据
    getDanganLeadingChange(params) {
        return $post('/admin/websit/incorruptibleRecordManager/updateByLD', params)
    },
    //领导机构删除
    getDanganLeadingClear(params) {
        return $post('/admin/websit/incorruptibleRecordManager/deleteLeadOrgan', params)
    },
    //领导机构新建
    getDanganLeadingAdd(params) {
        return $post('/admin/websit/incorruptibleRecordManager/saveLeadOrgan', params)
    },
    //领导机构上移
    getDanganLeadingup(params) {
        return $post('/admin/websit/incorruptibleRecordManager/moveUpLeadOrgan', params)
    },
    //领导机构下移
    getDanganLeadingDown(params) {
        return $post('/admin/websit/incorruptibleRecordManager/moveDownLeadOrgan', params)
    },
    //领导机构详情
    getDanganLeadingDETAILS(params) {
        return $post('/admin/websit/incorruptibleRecordManager/selectLeadOrganById', params)
    },
    //领导机构编辑保存的接口
    getDanganLeadingEditSave(params) {
        return $post('/admin/websit/incorruptibleRecordManager/updateLeadOrgan', params)
    },
    //领导机构编辑删除接口左侧
    getDanganLeadingEditLeftClear(params) {
        return $post('/admin/websit/incorruptibleRecordManager/deletePostName', params)
    },
    //领导机构编辑删除接口右侧
    getDanganLeadingEditRightClear(params) {
        return $post('/admin/websit/incorruptibleRecordManager/deletePersonName', params)
    },
    //-----------二级公司集团
    //集团列表
    geGroupList(params) {
        return $post('/admin/websit/incorruptibleRecordManager/selectAllByJT', params)
    },
    //集团上移
    getDanganGroupUp(params) {
        return $post('/admin/websit/incorruptibleRecordManager/moveUpByJT', params)
    },
    //集团下移
    getGroupDown(params) {
        return $post('/admin/websit/incorruptibleRecordManager/moveDownByJT', params)
    },
    //集团编辑修改
    getGroupChange(params) {
        return $post('/admin/websit/incorruptibleRecordManager/updateByJT', params)
    },
    //集团删除接口
    getGroupRemove(params) {
        return $post('/admin/websit/incorruptibleRecordManager/deleteByJT', params)
    },
    //集团新增接口
    getGroupAdd(params) {
        return $post('/admin/websit/incorruptibleRecordManager/saveEntityJT', params)
    },
    //廉政档案接口获取列表-二级公司
    getCompanyList() {
        return $post('/admin/websit/incorruptibleRecordManager/selectAllByGS')
    },
    //二级公司下移
    getCompanyDown(params) {
        return $post('/admin/websit/incorruptibleRecordManager/moveDownByGS', params)
    },
    //二级公司上移
    getCompanyup(params) {
        return $post('/admin/websit/incorruptibleRecordManager/moveUpByGS', params)
    },
    //二级公司编辑修改
    getCompanyChange(params) {
        return $post('/admin/websit/incorruptibleRecordManager/updateByGS', params)
    },
    //二级公司新增接口
    getCompanyAdd(params) {
        return $post('/admin/websit/incorruptibleRecordManager/saveEntityGS', params)
    },
    //二级公司删除接口
    getCompanyRemove(params) {
        return $post('/admin/websit/incorruptibleRecordManager/deleteByGS', params)
    },
    //=============================================================页面重构
    getleadinglist(params){
        return $post('/admin/websit/incorruptibleRecordManager/selectLeadOrgan', params)
    },
}




