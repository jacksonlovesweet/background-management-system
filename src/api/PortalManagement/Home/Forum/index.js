import {
    $get,
    $post
} from '@/utils/http'
// 首页情分论坛
export default {
    // 清风论坛获取列表
    getFroumList(params) {
        return $post('/admin/websit/portalPic/listForumPic', params)
    },
    // 清风论坛警用
    getFroumStop(params) {
        return $post('/admin/websit/portalPic/outOfServiceByQF', params)
    },
    // 清风论坛qiyong
    getFroumOpen(params) {
        return $post('/admin/websit/portalPic/enableQF', params)
    },
    // 清风论坛新建
    getFroumAdd(params) {
        return $post('/admin/websit/portalPic/saveForumPic', params)
    },
    // 清风论坛详情
    getFroumOneeach(params) {
        return $post('/admin/websit/portalPic/selectByQF', params)
    },
    // 清风论坛修改
    getFroumChange(params) {
        return $post('/admin/websit/portalPic/updateByIdQf', params)
    },

}




