import {
    $get,
    $post
} from '@/utils/http'
// 首页举报信息
export default {
    //举报信息获取列表
    getReportList(params) {
        return $post('/admin/websit/portalPic/listComplainPic', params)
    },
    // 举报信息警用
    getReportStop(params) {
        return $post('/admin/websit/portalPic/outOfServiceByJB', params)
    },
    // 举报信息qiyong
    getReportOpen(params) {
        return $post('/admin/websit/portalPic/enableJB', params)
    },
    // 举报信息新建
    getReportAdd(params) {
        return $post('/admin/websit/portalPic/saveComplainPic', params)
    },
    // 举报信息详情
    getReportEach(params) {
        return $post('/admin/websit/portalPic/selectByJB', params)
    },
    // 举报信息xiugai
    getReportChange(params) {
        return $post('/admin/websit/portalPic/updateComplainPic', params)
    },


}




