import {
    $get,
    $post
} from '@/utils/http'
// 友情链接接口
export default {
    // 友情链接列表展示
    getLinkList(params) {
        return $post('/admin/websit/outurl/listOutUrl', params)
    },
    // 友情链接上移接口
    getLinkUp(params) {
        return $post('/admin/websit/outurl/moveUp', params)
    },
    // 友情链接下移接口
    getLinkDown(params) {
        return $post('/admin/websit/outurl/moveDown', params)
    },
    // 友情链接删除接口
    getLinkClear(params) {
        return $post('/admin/websit/outurl/delOutUrl', params)
    },
    // 友情链接新建接口
    getLinkBuildNew(params) {
        return $post('/admin/websit/outurl/addOutUrl', params)
    },
    // 友情链接单个接口
    getLinkOne(params) {
        return $post('/admin/websit/outurl/getOutUrlById', params)
    },
    // 友情链接修改
    getLinkChange(params) {
        return $post('/admin/websit/outurl/editOutUrl', params)
    },
}