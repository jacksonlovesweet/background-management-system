import {
    $get,
    $post
} from '@/utils/http'
// 首页二维码
export default {
    //二维码页获取列表
    getQrCodeList(params) {
        return $post('/admin/websit/rqcode/listRqCode', params)
    },
    //二维码页上移
    getQrCodeUp(params) {
        return $post('/admin/websit/rqcode/moveUp', params)
    },
    //二维码页下移
    getQrCodeDowm(params) {
        return $post('/admin/websit/rqcode/moveDown', params)
    },
    //二维码页bianji 
    getQrCodeBianji(params) {
        return $post('/admin/websit/rqcode/editRqCode', params)
    },
    //二维码页xinjain
    getQrCodeBUild(params) {
        return $post('/admin/websit/rqcode/wzgl-sy-rqcode-xj', params)
    },
    //二维码页shanc jiek 
    getQrCodeClear(params) {
        return $post('/admin/websit/rqcode/delRqCode', params)
    },

    

}




