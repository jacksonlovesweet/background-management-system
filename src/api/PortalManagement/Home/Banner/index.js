import {
    $get,
    $post
} from '@/utils/http'
// 首页背景图及Banner图
export default {
    // 首页背景图及Banner图列表展示
    getBannerList(params) {
        return $post('/admin/websit/portalPic/listPortalPic', params)
    },
    // 首页背景图删除全部或一个
    getBannerListClear(params) {
        return $post('/admin/websit/portalPic/deleteByIdList', params)
    },
    // 首页背景图警用按钮
    getBannerStop(params) {
        return $post('/admin/websit/portalPic/outOfService', params)
    },
    // 首页背景图启动
    getBannerOpen(params) {
        return $post('/admin/websit/portalPic/enable', params)
    },
    // 首页xinzhangr背景图
    getBannerAdd(params) {
        return $post('/admin/websit/portalPic/savePortalPic', params)
    },
    // 首页编辑背景图片
    getBannerBianji(params) {
        return $post('/admin/websit/portalPic/updateByIdBanner', params)
    },
    // 首页编辑背景图-请求单个数据
    getBannerOne(params) {
        return $post('/admin/websit/portalPic/selectById', params)
    },
    //下拉框选择后触发的方法
    getBannersort(params){
        return $post('/admin/websit/portalPic/getMaxSort', params)
    }
}




