import {
    $get,
    $post
} from '@/utils/http'
// 首页轮播页配置
export default {
    //轮播页获取列表
    getRotationList(params) {
        return $post('/admin/websit/rotationChartManager/listPic', params)
    },
    //轮播页审核通过接口
    getRotationAdopt(params) {
        return $post('/admin/websit/rotationChartManager/auditPic', params)
    },
    //轮播页编辑接口查询渲染
    getRotationBianJi(params) {
        return $post('/admin/websit/rotationChartManager/getPicById', params)
    },
    //轮播页编辑页面提交按钮
    getRotationSubmit(params) {
        return $post('/admin/websit/rotationChartManager/editPic', params)
    },
    //轮播页编辑页面发布接口
    getRotationBtSend(params) {
        return $post('/admin/websit/rotationChartManager/publishArticle', params)
    },
    //正价单条
    getRotationBuild(params) {
        return $post('/admin/websit/rotationChartManager/addPic', params)
    },
    //xiagiqng
    getRotationDetail(params) {
        return $post('/admin/websit/rotationChartManager/getPicById', params)
    },
    

}




