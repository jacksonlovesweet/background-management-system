import {
    $get,
    $post
} from '@/utils/http'
//角色管理
export default {

    //  组织机构分类列表
    getRole(params) {

        return $post('/admin/sys/role/pagelist', params)

    },
    searchRole(params) {

        return $post('/admin/sys/role/pagelist', params)

    },
    // 新建
    newRole(params) {

        return $post('/admin/sys/role/add', params)

    },
    // 编辑
    editRole(params) {

        return $post('/admin/sys/role/detail', params)

    },
    // 提交
    submitRole(params) {

        return $post('/admin/sys/role/edit', params)

    },
    // 人员名单
    personRole(params) {

        return $post('/admin/sys/role/user-list', params)

    },
    // 查询
    personRoleSearch(params) {

        return $post('/admin/sys/role/user-list', params)

    },
    //删除
    deleteRole(params) {

        return $post('/admin/sys/role/remove', params)

    },
    //停用
    stopRole(params) {

        return $post('/admin/sys/role/disable', params)

    },
    //启用用
    startRole(params) {

        return $post('/admin/sys/role/enable', params)

    },
    //    角色资源树
    treeRole(params) {

        return $post('/admin/sys/role/res-tree', params)

    },
  //    角色公司树
  companyRole(params) {

    return $post('/admin/sys/role/company-tree', params)

    },
//   权限分配
resetRole(params) {
    return $post('/admin/sys/role/authority', params)

    },
}