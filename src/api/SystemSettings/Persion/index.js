import {
    $get,
    $post
} from '@/utils/http'
// 个人中心
export default {
    //个人详情
    getPersion() {
        return $post('/admin/sys/self/detail')
    },
    //编辑个人详情
    getPersionChange(params) {
        return $post('/admin/sys/self/edit',params)
    },
    //修改密码
    getChangePass(params) {
        return $post('/admin/sys/self/modifyPwd',params)
    },

}




