import {
    $get,
    $post
} from '@/utils/http'
//用户管理
export default {

    //  组织机构分类列表
    getUser(params) {

        return $post('/admin/sys/user/pagelist', params)

    },
//   搜索
searchUser(params) {

    return $post('/admin/sys/user/pagelist', params)

    },
    //   搜索
searchUser1(params) {

    return $post('/admin/sys/user/pagelist', params)

    },
// 用户详情
editUser(params) {

    return $post('/admin/sys/user/detail', params)

    },
    // 删除
   deleteUser(params) {

        return $post('/admin/sys/user/remove', params)
    
    },
     //停用
     stopUser(params) {

        return $post('/admin/sys/user/disable', params)

    },
     //启用
     startUser(params) {

        return $post('/admin/sys/user/enable', params)

    },
      //重置
     resetUser(params) {

        return $post('/admin/sys/user/pwd-reset', params)

    },
    //  部门树
    treeUser() {
        return $post('/admin/sys/user/org-tree')

    },
      // 角色下拉列表
      downUser(params) {
        return $post('/admin/sys/user/role-list',params)

    },
    //   新增
    newUser(params) {
        return $post('/admin/sys/user/add',params)
 
    },
    // 详情
    detailUser(params) {
        return $post('/admin/sys/user/detail',params)
 
    },
    //  编辑
    editwUser(params) {
        return $post('/admin/sys/user/edit',params)
 
    },
}