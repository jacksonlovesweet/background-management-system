import {
    $get,
    $post
} from '@/utils/http'
// 字典管理
export default {
    //字典左边列表
    getDictionariesleftList(params) {
        return $post('/admin/sys/dict/type-list',params)
    },
    //字典右边列表
    getDictionariesRightList(params) {
        return $post('/admin/sys/dict/item-list',params)
    },
    //字典右边删除
    getDictionariesClear(params) {
        return $post('/admin/sys/dict/item-remove',params)
    },
    //字典左边详情类型
    getLeftEvery(params) {
        return $post('/admin/sys/dict/type-detail',params)
    },
    //字典编辑类型
    getDictionariesBianji(params) {
        return $post('/admin/sys/dict/type-edit',params)
    },
    //字典项详情you边
    getDictionariesEach(params) {
        return $post('/admin/sys/dict/item-detail',params)
    },
    //字典项编辑
    getDictionariesBianJIrIGHT(params) {
        return $post('/admin/sys/dict/item-edit',params)
    },
    //字典项新建
    getDictionariesNew(params) {
        return $post('/admin/sys/dict/item-add',params)
    },
}




