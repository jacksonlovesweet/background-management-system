import {
    $get,
    $post
  } from '@/utils/http'
  
  export default {
    // 用户登陆接口
    getLoginIp(params) {
      return $post('/admin/sys/self/login',params)
    },
    //获取个人详情
    getpersonDetail(params) {
      return $post('/admin/sys/self/detail',params)
    },
    //获取登出
    getpersonOut(params) {
      return $post('/admin/sys/self/logout',params)
    },
  }


  
  
  
  