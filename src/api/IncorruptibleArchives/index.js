import {
    $get,
    $post
} from '@/utils/http'
//廉政档案
export default {
    //监督对象===============================================
    // 监督对象公司列表
    getSuperviseList(params) {
        return $post('/admin/lzda/jddx/pagelist', params)
    },
    // 监督对象公司下拉列表
    getSuperviseCompanyList(params) {
        return $post('/admin/lzda/jddx/company-list', params)
    },
    // 监督对象公司列表批量删除
    getSuperviseClear(params) {
        return $post('/admin/lzda/jddx/remove', params)
    },
    // 监督对象公司列表批量导入
    getSuperviseImport(params) {
        return $post('/admin/lzda/jddx/import', params)
    },

    // 分页查询公司列表
    pagelist(params) {
        return $post('/admin/lzda/gsda/pagelist', params)
    },
    // 查询公司名称列表
    companylist(params) {
        return $post('/admin/lzda/jddx/company-list', params)
    },
    // 查询公司档案列表
    archivelist(params) {
        return $post('/admin/lzda/gsda/archive-list', params)
    },
    // 查询公司信访举报列表
    complainlist(params) {
        return $post('/admin/lzda/gsda/complain-list', params)
    },
    // 查询公司专项监督列表
    taskReportlist(params) {
        return $post('/admin/lzda/gsda/task-report-list', params)
    },
    // 查询公司日常监督列表
    rectifyReportlist(params) {
        return $post('/admin/lzda/gsda/rectify-report-list', params)
    },
    // 查询公司巡视巡察列表
    problemlist(params) {
        return $post('/admin/lzda/gsda/problem-list', params)
    },
    // 监督对象-新建监督对象
    getSuperviseBuildNewperson(params) {
        return $post('/admin/lzda/jddx/add', params)
    },
    // 监督对象-详情页，获取基本信息展示
    getSuperviseBaseDetail(params) {
        return $post('/admin/lzda/jddx/basedetail', params)
    },
    // 监督对象-详情页，获取完整信息展示
    getSuperviseBaseDetailAll(params) {
        return $post('/admin/lzda/jddx/completedetail', params)
    },
    // 监督对象-详情页，获取档案列表
    getSuperviseArchivesList(params) {
        return $post('/admin/lzda/jddx/archive-list', params)
    },
    // 监督对象-详情页，信访举报列表
    getSuperviseReportList(params) {
        return $post('/admin/lzda/jddx/complain-list', params)
    },
    // 监督对象-详情页，信访举报列表
    getSuperviseEditList(params) {
        return $post('/admin/lzda/jddx/edit', params)
    },
    // 监督对象-详情页，档案列表更多
    getSuperviseArchiveMore(params) {
        return $post('/admin/lzda/jddx/archive-more', params)
    },
    // 监督对象-详情页，信访举报列表更多
    getSuperviseComplainMore(params) {
        return $post('/admin/lzda/jddx/complain-more', params)
    },
    // 监督对象-详情页，删除档案的列表项
    getSuperviseArchiveRemove(params) {
        return $post('/admin/lzda/jddx/archive-remove', params)
    },
    // 监督对象-详情页，新建干部任免审批表
    getSuperviseRmspbAdd(params) {
        return $post('/admin/lzda/jddx/rmspb-add', params)
    },
    // 监督对象-详情页，编辑干部任免审批表
    getSuperviseRmspbEdit(params) {
        return $post('/admin/lzda/jddx/rmspb-edit', params)
    },
    // 监督对象-详情页，查看干部任免审批表
    getSuperviseRmspbDetail(params) {
        return $post('/admin/lzda/jddx/rmspb-detail', params)
    },
    // 监督对象-详情页，操办婚丧喜庆事宜报告表-新建
    getSuperviseHsxqbAdd(params) {
        return $post('/admin/lzda/jddx/hsxqb-add', params)
    },
    // 监督对象-详情页，操办婚丧喜庆事宜报告表-详情
    getSuperviseHsxqbDetail(params) {
        return $post('/admin/lzda/jddx/hsxqb-detail', params)
    },
    // 监督对象-详情页，操办婚丧喜庆事宜报告表-编辑提交
    getSuperviseHsxqbEdit(params) {
        return $post('/admin/lzda/jddx/hsxqb-edit', params)
    },
    // 新建公司档案
    archiveAdd(params) {
        return $post('/admin/lzda/gsda/archive-add', params)
    },
    //公司更多档案列表
    archiveMore(params) {
        return $post('/admin/lzda/gsda/archive-more', params)
    },
    //查看公司档案
    archiveDetail(params) {
        return $post('/admin/lzda/gsda/archive-detail', params)
    },
    //编辑公司档案
    archiveEdit(params) {
        return $post('/admin/lzda/gsda/archive-edit', params)
    },
    //删除公司档案
    archiveRemove(params) {
        return $post('/admin/lzda/gsda/archive-remove', params)
    },
    //公司完整信息详情
    compeletedetail(params) {
        return $post('/admin/lzda/gsda/compeletedetail', params)
    },
    //公司完整信息详情
    compeletEdit(params) {
        return $post('/admin/lzda/gsda/edit', params)
    },
    //公司完整信息修改
    compeletInfoEdit(params) {
        return $post('/admin/lzda/gsda/edit', params)
    },
    //公司信访举报更多列表
    complainMore(params) {
        return $post('/admin/lzda/gsda/complain-more', params)
    },
    //公司信访举详情
    complainDetail(params) {
        return $post('/admin/lzda/gsda/complain-detail', params)
    },
    //公司专项监督更多列表
    taskReportMore(params) {
        return $post('/admin/lzda/gsda/task-report-more', params)
    },
    //公司专项监督更多列表
    taskReportDetail(params) {
        return $post('/admin/lzda/gsda/task-report-detail', params)
    },
    //公司日常监督更多列表
    rectifyReportMore(params) {
        return $post('/admin/lzda/gsda/rectify-report-more', params)
    },
    //公司巡视巡查更多列表
    problemMore(params) {
        return $post('/admin/lzda/gsda/problem-more', params)
    },
    //公司巡视巡查详情
    problemDetail(params) {
        return $post('/admin/lzda/gsda/problem-detail', params)
    },
}