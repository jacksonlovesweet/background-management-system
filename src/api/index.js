// 门户管理页面
// 风险防控
import risk from './PortalManagement/risk/risk.js'
//党纪法规
import party from './PortalManagement/party/party.js'
// 巡视巡察
import look from './PortalManagement/look/look.js'
//审查调查
import review from './PortalManagement/review/review.js'
//调查评估
import InvestigationAndEvaluation from './PortalManagement/InvestigationAndEvaluation'
// 首页
//友情链接
import Link from './PortalManagement/Home/Links'
//Banner图
import Banner from './PortalManagement/Home/Banner'
//清风论坛
import Forum from './PortalManagement/Home/Forum'
//举报信息
import Report from './PortalManagement/Home/Report'
//轮播页配置
import Rotation from './PortalManagement/Home/Rotation'
//二维码
import QrCode from './PortalManagement/Home/QrCode'
//系统简介
import Introduction from './PortalManagement/Introduction'
//廉政档案
import DangAn from './PortalManagement/DangAn'
//网上举报
import ReportOne from './PortalManagement/ReportOne'
//清风论坛
import ForumBig from './PortalManagement/ForumBig'

//公共地址
//图片获取路径

// 系统设置页面
//个人中心
import Persion from './SystemSettings/Persion'
import Dictionaries from './SystemSettings/Dictionaries'

import tissue from './SystemSettings/tissue/tissue.js'
// 角色管理
import role from './SystemSettings/role/role.js'
// 用户管理
import user from './SystemSettings/user/user.js'
// 用户登陆
import Login from './Login'
import common from './common'
// 巡视巡察
// 问题清单
import haveLook from './haveLook/haveLook.js'

// 监督举报
// 动态监督
// 三公经费
import Expenditure from './SuperviseAndReport/DynamicMonitoring/Expenditure/Expenditure.js'
// 三重一大
import Conference from './SuperviseAndReport/DynamicMonitoring/Conference/Conference.js'
// 专项督查
// 任务管理
import Task from './SuperviseAndReport/SpecialSupervision/Task/task.js'

// 日常监督
// 情况通报
import Notification from './SuperviseAndReport/Daily/Notification/Notification.js'
// 整改汇报
import Neaten from './SuperviseAndReport/Daily/Neaten/Neaten.js'
// 信访举报
import PetitionReport from './SuperviseAndReport/PetitionReport/PetitionReport.js'
// 请示报告
import Instructions from './SuperviseAndReport/Instructions/Instructions.js'
//廉政档案
import IncorruptibleArchives from './IncorruptibleArchives/index'
const Api = Object.assign({}, risk, Link, common, Banner, party, Forum, Report, Rotation, QrCode, Introduction, DangAn, look, review, ReportOne, ForumBig, tissue, role, Persion, Dictionaries, user, haveLook, Login, Task, Neaten, Notification, PetitionReport, Instructions, Conference, Expenditure,IncorruptibleArchives, InvestigationAndEvaluation)
export default Api