import {
    $get,
    $post
} from '@/utils/http'
// 专项监督 - 信访举报
export default {
    // 公司名称下拉列表
    getComplainCompanyList() {
        return $get('/admin/supervise/complain/company-list')
    },
    // 信访举报分页列表
    getComplainPage(data) {
        return $post('/admin/supervise/complain/page', data)
    },
    // 导出信访举报列表
    getComplainExport(data) {
        return $post('/admin/supervise/complain/export', data, null, { responseType: 'blob' })
    },
    // /信访举报详情
    getComplainDetail(data) {
        return $post('/admin/supervise/complain/detail', data)
    },
    // 审批信访举报
    doComplainApprove(data) {
        return $post('/admin/supervise/complain/approve', data)
    },
}