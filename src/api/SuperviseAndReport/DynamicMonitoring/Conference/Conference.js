import {
    $get,
    $post
} from '@/utils/http'
// 动态监督 - 三重一大
export default {
    // 公司名称下拉列表
    getMeetingCompanyList() {
        return $get('/admin/supervise/meeting/manage/company-list')
    },
    // 管理端会议分页列表
    getMeetingManagePage(data) {
        return $post('/admin/supervise/meeting/manage/page', data)
    },
    // 上传会议
    doMeetingAdd(data) {
        return $post('/admin/supervise/meeting/upload/add', data)
    },
    // 删除会议
    doMeetingDelete(data) {
        return $post('/admin/supervise/meeting/upload/delete', data)
    },
    // 上传端会议分页列表
    getMeetingUploadPage(data) {
        return $post('/admin/supervise/meeting/upload/page', data)
    },
    // 下载会议附件
    doMeetingDownload(data) {
        return $post('/admin/supervise/meeting/download', data, null, {responseType:'blob'})
    },
    // 预览会议文件
    doMeetingPreview(data) {
        return $post('/admin/supervise/meeting/preview', data)
    },
}