import {
    $get,
    $post
} from '@/utils/http'
// 动态监督 - 三公消费
export default {
    // 公司/部门查询
    getTpConsumptionGetOrg() {
        return $post('/admin/supervise/tpConsumption/getOrg')
    },
    // 科目查询
    getTpConsumptionGetFees(data) {
        return $post('/admin/supervise/tpConsumption/getFees')
    },
    // 三公消费查询
    gettpConsumptionQuery(data) {
        return $post('/admin/supervise/tpConsumption/query', data)
    },
    // 动态监督-三公消费统计-走势图
    gettpConsumptionGetTrend(data) {
        return $post('/admin/supervise/tpConsumption/getTrend', data)
    },
    // 动态监督-三公消费统计-各类别占比
    gettpConsumptiongGetCategory(data) {
        return $post('/admin/supervise/tpConsumption/getCategory', data)
    },
    // 动态监督 - 三公消费统计 - 分类明细
    gettpConsumptiongGetClassification(data) {
        return $post('/admin/supervise/tpConsumption/getClassification', data)
    },
    //  动态监督 - 财务监督
    // 财务监督-资产变动查询
    getAssetsChangeQuery(data) {
        return $post('/admin/supervise/assetsChange/query', data)
    },
    // 财务监督-现金流量查询
    getCashFlowQuery(data) {
        return $post('/admin/supervise/cashFlow/query', data)
    },
    // 财务监督-利润查询
    getProfitQuery(data) {
        return $post('/admin/supervise/profit/query', data)
    },
    // 财务监督-资产变动查询
    getAssetsChangeQueryList(data) {
        return $post('/admin/supervise/assetsChange/queryChat', data)
    },
    // 财务监督-现金流量查询
    getCashFlowQueryList(data) {
        return $post('/admin/supervise/cashFlow/queryChat', data)
    },
    // 财务监督-利润查询
    getProfitQueryList(data) {
        return $post('/admin/supervise/profit/queryChat', data)
    },

}