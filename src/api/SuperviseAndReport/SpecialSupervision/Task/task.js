import {
    $get,
    $post
} from '@/utils/http'
// 专项监督 - 任务管理
export default {
    // 管理端-条件分页查询专项监督任务列表
    getSpecialTaskList(data) {
        return $post('/admin/supervise/task/manage/page', data)
    },
    // 管理端-公司列表查询
    getSpecialOrglist(data) {
        return $get('/admin/supervise/task/manage/org-list')
    },
    // 管理端-创建专项监督任务
    doSpecialAdd(data) {
        return $post('/admin/supervise/task/manage/add', data)
    },
    // 管理端-任务详情查询
    getSpecialTaskinfo(data) {
        return $post('/admin/supervise/task/manage/detail', data)
    },
    // 专项任务催办
    getTaskManageUrging(data) {
        return $post('/admin/supervise/task/manage/urging', data)
    },
    // 催办任务
    doPresstask(data) {
        return $post('/admin/supervise/task/manage/urging', data)
    },

    //专项任务汇报分页列表 
    getTaskDetailReportPage(data) {
        return $post('/admin/supervise/task/manage/detail/report-page', data)
    },
    // 专项任务汇报详情
    getTaskDetailReportDetail(data) {
        return $post('/admin/supervise/task/manage/detail/report-detail', data)
    },
    // 专项任务汇报催办
    doTaskDetailReportUrging(data) {
        return $post('/admin/supervise/task/manage/detail/report-urging', data)
    },




    // 上传端-条件分页查询汇报任务列表
    getSpecialReportList(data) {
        return $post('/admin/supervise/task/upload/page', data)
    },
    // 上传端-任务详情查询
    getSpecialReportinfo(data) {
        return $post('/admin/supervise/task/upload/detail', data)
    },
    // 上传端-汇报
    doSpecialReport(data) {
        return $post('/admin/supervise/task/upload/submit', data)
    },







    // 单位/部门下拉列表
    getSituationOrgList(data) {
        return $get('/admin/supervise/situation/org-list')
    },
    // 情况报告分页列表
    getSpecialReportedlist(data) {
        return $post('/admin/supervise/situation/page', data)
    },
    // 情况报告详情
    getSuperviseSituationDetail(data) {
        return $post('/admin/supervise/situation/detail', data)
    },
    // 下载情况报告
    getSuperviseSituationDownload(data) {
        return $post('/admin/supervise/situation/download', data)
    },
}