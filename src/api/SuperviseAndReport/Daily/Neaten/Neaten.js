import {
    $get,
    $post
} from '@/utils/http'
// 日常监督 - 整理汇报
export default {
    // 单位/部门下拉列表
    getRectifyOrglist() {
        return $get('/admin/supervise/rectify/org-list')
    },
    // 整改汇报分页列表
    getRectifyPage(data) {
        return $post('/admin/supervise/rectify/page', data)
    },
    // 整改汇报详情
    getRectifyDetail(data) {
        return $post('/admin/supervise/rectify/detail', data)
    }
}