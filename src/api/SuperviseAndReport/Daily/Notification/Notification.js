import {
    $get,
    $post
} from '@/utils/http'
// 日常监督 - 情况通报
export default {
    // 情况通报汇报分页列表 (管理端)
    getManageReportPage(data) {
        return $post('/admin/supervise/notification/manage/page', data)
    },
    // 通报范围下拉列表
    getNotificationOrgList(data) {
        return $get('/admin/supervise/notification/manage/org-list')
    },
    // 接收单位匹配列表
    getOrgMatchingList(data) {
        return $post('/admin/supervise/notification/manage/org-matching-list', data)
    },
    // 创建情况通报
    doNotificationManageAdd(data) {
        return $post('/admin/supervise/notification/manage/add', data)
    },
    // 情况通报详情
    getNotificationDetail(data) {
        return $post('/admin/supervise/notification/manage/detail', data)
    },
    // 情况通报汇报分页列表
    getNotificationReportPage(data) {
        return $post('/admin/supervise/notification/manage/detail/report-page', data)
    },
    // 情况通报汇报详情
    getNotificationReportDetail(data) {
        return $post('/admin/supervise/notification/manage/detail/report-detail', data)
    },

    // 情况通报汇报分页列表（上传端）
    getUploadPage(data) {
        return $post('/admin/supervise/notification/upload/page', data)
    },
    // 提交情况通报汇报
    doNotificationReportAdd(data) {
        return $post('/admin/supervise/notification/upload/submit', data)
    },
    // 校验情况通报汇报
    doNotificationReportCheck(data) {
        return $post('/admin/supervise/notification/upload/check', data)
    },
    // 情况通报详情
    getNotificationReportUserDetail(data) {
        return $post('/admin/supervise/notification/upload/detail', data)
    },
    // 情况通报催办
    doNotificationManageUrging(data) {
        return $post('/admin/supervise/notification/manage/urging', data)
    },
    // 情况通报催办
    doNotificationManageReportUrging(data) {
        return $post('/admin/supervise/notification/manage/detail/report-urging', data)
    },

}