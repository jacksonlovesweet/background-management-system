import {
    $get,
    $post
} from '@/utils/http'
// 专项监督 - 请示报告
export default {
    // 公管理端-公司列表查询
    getInstructionsOrglist() {
        return $post('/admin/supervise/instructions/orglist')
    },
    // 管理端-条件列表查询
    getInstructionsList(data) {
        return $post('/admin/supervise/instructions/list', data)
    },
    // 上报端-上报列表条件查询
    getInstructionsReportlist(data) {
        return $post('/admin/supervise/instructions/reportlist', data)
    },
    // 上报端-请示报告新增
    getInstructionsAdd(data) {
        return $post('/admin/supervise/instructions/add', data)
    },
}