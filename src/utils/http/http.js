import axios from 'axios'
import router from '@/router'
import {
  MessageBox,
  Message
} from 'element-ui'
import {
  getToken,
  removeToken
} from '@/utils/auth'

const instance = axios.create({
  // timeout: 6000,
  baseURL: '/'
})
// 添加请求拦截器
instance.interceptors.request.use(config => {
  config.headers['client_id'] = 'web'
  config.headers['client_secret'] = 'web'
  config.headers['deviceId'] = 'web'
  // 巡视巡查
  // config.headers['token'] = '51df1852-da0b-458e-a3c0-ee72aa743250'
  //超级管理员token
  // config.headers['token'] = '33277eae-7afd-4dcc-8c23-129b5a969692'
  // UTA环境
  // config.headers['token'] = '5b827eaa-c8b6-4af6-b82b-976813fb2f16'
  //获取本地的token值
  const token = window.localStorage.getItem("LoginToken");
  config.headers['token'] = token
  // if (getToken()) {
  //   config.headers['Authorization'] = getToken()
  // } else {
  //   config.headers['Authorization'] = 1
  // }

  return config
}, error => {
  Promise.reject(error)
})
// 添加响应拦截器
instance.interceptors.response.use(response => {
  const res = response.data
  // console.log(response,"response")
  // console.log(res,"res")
//res.code == '402' || res.code == '403' || res.code == '405' || res.code == '100'
  if ( res.code === '500' ) {
    Message({
      message: "登陆失败请联系管理员",
      type: 'warning',
      duration: 3 * 1000
    })
  } else if (res.code === '400' || res.code === '401') {
    sessionStorage.setItem("is_true_login", '{"istrue":"true"}')
    let arr = sessionStorage.getItem("is_true_login")
    let newvalue = JSON.parse(arr);
    console.log(typeof (newvalue.istrue))
    if (newvalue.istrue === "true") {
      sessionStorage.setItem('is_true_login', "{istrue:false}")
      console.log("11111111111111")
      MessageBox.confirm('登陆失效，请回登陆页重新登录', '温馨提示', {
        confirmButtonText: '登录',
        showCancelButton: false,
        closeOnClickModal: false,
        type: 'warning'
      }).then(() => {
        window.localStorage.removeItem("LoginToken");
        router.push({
          name: 'login',
        })
      }).catch(() => {
        window.localStorage.removeItem("LoginToken");
      })
    } else {
      console.log(isTrue, "denglushibai")
      return
    }


  } else {
    let str = JSON.stringify({ istrue: true });
    sessionStorage.setItem('is_true_login', str)
    return res
  }
}, error => {
  console.log(error,"cuowu")
  // 401 query记录当前路由并跳转至登录页
  if (error.response.status == '500') {
    // MessageBox.confirm('您已经登录超时，请回首页重新登录', '温馨提示', {
    //   confirmButtonText: '登录',
    //   showCancelButton: false,
    //   closeOnClickModal: false,
    //   type: 'warning'
    // }).then(() => {
    //   // removeToken()
    //   window.localStorage.removeItem("LoginToken");
    //   const {
    //     fullPath: redirect
    //   } = router.currentRoute
    //   router.push({
    //     name: 'login',
    //     query: {
    //       redirect
    //     }
    //   })
    // }).catch(() => {
    //   // removeToken()
    //   window.localStorage.removeItem("LoginToken");
    // })
    Message({
      message: "后台服务未启动，请联系管理员",
      type: 'warning',
      duration: 3 * 1000
    })
    return
  }
  let data = error.response.data.data
  let msg
  if (data && typeof data == 'string') {
    data = JSON.parse(data) || {}
    msg = data.msg
  }
  Message({
    message: msg || '请求失败',
    type: 'error',
    duration: 3500
  })
})
export default instance
