module.exports = {
    devServer: {
        port: 8889,
        open: true,
        proxy: {
            "/api": {
                //UAT环境
                // target: "http://192.168.2.33:8080/",
                //联调环境
                target: "http://192.168.2.34:8080/",
                changeOrign: true,
                pathRewrite: {
                    "^/api": ""
                }
            }
        }
    },
    lintOnSave: false, //彻底关闭eslint
}